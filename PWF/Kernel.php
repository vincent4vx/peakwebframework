<?php

/*
 * The MIT License
 *
 * Copyright 2015 Vincent Quatrevieux <quatrevieux.vincent@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace PWF;

use PWF\Input\HttpInput;
use PWF\Output\Output;

/**
 *
 * @author Vincent Quatrevieux <quatrevieux.vincent@gmail.com>
 * @property-read Loader $loader The Loader
 * @property-read array $config Configuration
 */
interface Kernel {
    /**
     * Get a value into the registry
     * @param string $name
     * @return mixed Return the registered value, or null
     */
    public function __get($name);
    
    /**
     * Set a new value into the registry
     * @param string $name
     * @param mixed $value
     * @throws Exception\InvalidOperationException when a value is already set
     */
    public function __set($name, $value);
    
    /**
     * Check if a value exists in the registry
     * @param string $name
     * @return boolean true if exists, false in other cases
     */
    public function __isset($name);
    
    /**
     * Set a value into registry without check if a value is already set
     * @param string $name
     * @param mixed $value
     */
    public function forceSet($name, $value);

    /**
     * Get a configuration item
     * @param string $key The config key, seperated with "."
     * @param mixed $default Default value with item is not found
     * @return mixed
     */
    public function conf($key, $default = null);
    
    /**
     * Run the application
     * @param string $controller The controller name
     * @param string $action The action to perform
     * @param array $args the arguments
     * @return Output\InternalOutput The Output of the application
     * @deprecated Use execute instead
     */
    public function run($controller, $action, array $args = []);
    
    /**
     * Execute the controller using the router
     * @param HttpInput|null $input The input instance or null to use the app Input
     * @param Output|null $output The output instance or null to use the app Output
     * @return Output The output instance after execution
     */
    public function execute(HttpInput $input = null, Output $output = null);
}
