<?php

/*
 * The MIT License
 *
 * Copyright 2015 Vincent Quatrevieux <quatrevieux.vincent@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace PWF\Cache;

/**
 * Description of DummyCache
 *
 * @author Vincent Quatrevieux <quatrevieux.vincent@gmail.com>
 */
class DummyCache implements Cache{
    private $data = [];
    
    public function clearAll() {
        $this->data = [];
    }

    public function gc() {}

    public function getAll() {
        return $this->data;
    }

    public function remove($key) {
        unset($this->data[$key]);
    }

    public function retrieve($key) {
        return isset($this->data[$key]) ? $this->data[$key] : null;
    }

    public function store($key, $value, $duration = 60) {
        $this->data[$key] = $value;
    }
    
    public function exists($key) {
        return array_key_exists($key, $this->data);
    }

}
