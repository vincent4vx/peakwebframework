<?php

/*
 * The MIT License
 *
 * Copyright 2015 Vincent Quatrevieux <quatrevieux.vincent@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace PWF\Cache;

/**
 *
 * @author Vincent Quatrevieux <quatrevieux.vincent@gmail.com>
 */
interface Cache {
    /**
     * Store data into the cache
     * @param string $key The data key
     * @param mixed $value The value
     * @param int $duration The cache duration in seconds
     */
    public function store($key, $value, $duration = 60);
    
    /**
     * Get the stored data
     * @param string $key The data key
     * @return mixed|null Return the data, or null if not present (or invalid)
     */
    public function retrieve($key);
    
    /**
     * Remove the data from cache
     * @param string $key The data key
     */
    public function remove($key);
    
    /**
     * Check if the data exists into the cache
     * @param string $key The data key
     * @return bool true if exists, or false
     */
    public function exists($key);
    
    /**
     * Remove all data from cache
     */
    public function clearAll();
    
    /**
     * Get all data from cache
     * @return array The data indexed by the cache key
     */
    public function getAll();
    
    /**
     * Clear from cache the invalid data (expired)
     */
    public function gc();
}
