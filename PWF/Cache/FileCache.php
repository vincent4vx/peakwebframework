<?php

/*
 * The MIT License
 *
 * Copyright 2015 Vincent Quatrevieux <quatrevieux.vincent@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace PWF\Cache;

/**
 * Description of FileCache
 *
 * @author Vincent Quatrevieux <quatrevieux.vincent@gmail.com>
 */
class FileCache implements Cache{
    private $dir;
    private $data = [];
    
    public function __construct(\PWF\Kernel $kernel) {
        $this->dir = $kernel->conf('cache.data_dir', __DIR__ . DIRECTORY_SEPARATOR . 'data' . DIRECTORY_SEPARATOR);
        
        if(!is_dir($this->dir)){
            if(!@mkdir($this->dir, 0700, true)){
                $error = error_get_last();
                throw new \ErrorException(__CLASS__ . ' : ' . $error['message'], 0, $error['type'], $error['file'], $error['line'], null);
            }
        }
    }
    
    public function clearAll() {
        foreach(scandir($this->dir) as $file){
            if(is_file($this->dir . $file))
                unlink($this->dir . $file);
        }
        
        $this->data = [];
    }

    public function exists($key) {
        if(array_key_exists($key, $this->data))
            return true;
        
        return $this->setFileData($key);
    }
    
    private function setFileData($key){
        $file = $this->dir . base64_encode($key);
        if(!is_file($file))
            return false;
        
        $data = file_get_contents($file);
        $data = unserialize($data);
        
        if(time() > $data['expiration']){
            unlink($file);
            return false;
        }
        
        $this->data[$key] = $data['data'];
        return true;
    }

    public function gc() {
        foreach(scandir($this->dir) as $file){
            $realFile = $this->dir . $file;
            
            if(!is_file($realFile))
                continue;
            
            $expiration = unserialize(file_get_contents($realFile));
            
            if(time() > $expiration)
                unlink($realFile);
        }
    }

    public function getAll() {
        foreach(scandir($this->dir) as $file){
            $key = base64_decode($file);
            $this->exists($key);
        }
        
        return $this->data;
    }

    public function remove($key) {
        while(is_file($this->dir . base64_encode($key))){
            unlink($this->dir . base64_encode($key));
        }
        
        unset($this->data[$key]);
    }

    public function retrieve($key) {
        if($this->exists($key))
            return $this->data[$key];
        
        return null;
    }

    public function store($key, $value, $duration = 60) {
        $this->data[$key] = $value;
        
        $file = $this->dir . base64_encode($key);
        $data = [
            'expiration' => time() + $duration,
            'data' => $value
        ];
        
        file_put_contents($file, serialize($data));
    }
}
