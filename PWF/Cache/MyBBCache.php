<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace PWF\Cache;

/**
 * Cache adapter for MyBB cache system
 *
 * @author vincent
 */
class MyBBCache implements Cache{
    /**
     *
     * @var \datacache
     */
    private $datacache;
    
    public function __construct(\datacache $datacache) {
        $this->datacache = $datacache;
    }
    
    public function clearAll() {
        throw new \PWF\Exception\InvalidOperationException(__METHOD__ . ' is not yet supported in MyBBCache');
    }

    public function exists($key) {
        return $this->datacache->read($key) !== false;
    }

    public function gc() {
        throw new \PWF\Exception\InvalidOperationException(__METHOD__ . ' is not yet supported in MyBBCache');
    }

    public function getAll() {
        return $this->datacache->cache;
    }

    public function remove($key) {
        $this->datacache->update($key, false);
    }

    public function retrieve($key) {
        $this->datacache->read($key);
    }

    public function store($key, $value, $duration = 60) {
        $this->datacache->update($key, $value);
    }
}
