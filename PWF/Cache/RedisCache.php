<?php

/*
 * The MIT License
 *
 * Copyright 2016 vincent.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace PWF\Cache;

use Database\Redis\RedisDatabase;
use PWF\Database\DatabaseHandler;
use PWF\Kernel;

/**
 * Cache driver for redis database
 * @author vincent
 */
class RedisCache implements Cache{
    /**
     *
     * @var RedisDatabase
     */
    private $db;
    
    private $config;
    
    private $data = [];
    
    public function __construct(Kernel $kernel, DatabaseHandler $handler) {
        $this->config = $kernel->conf('cache', [
            'redis_db' => 'default',
            'prefix' => 'pwf_cache_'
        ]);
        
        $this->db = $handler->getRedisConnection($this->config['redis_db']);
    }
    
    public function clearAll() {
        $keys = $this->$this->db->keys($this->config['prefix'] . '*');
        
        if(!empty($keys))
            $this->db->del($keys);
        
        $this->data = [];
    }

    public function exists($key) {
        return array_key_exists($key, $this->data) || $this->db->exists($this->config['prefix'] . $key);
    }

    /**
     * Redis automatically run GC
     */
    public function gc() {}

    public function getAll() {
        $keys = $this->db->keys($this->config['prefix'] . '*');
        $values = $this->db->getMultiple($keys);
        
        $len = count($keys);
        $prefixLen = strlen($this->config['prefix']);
        for($i = 0; $i < $len; ++$i){
            $key = substr($keys[$i], $prefixLen);
            $this->data[$key] = $values[$i];
        }
        
        return $this->data;
    }

    public function remove($key) {
        unset($this->data[$key]);
        $this->db->del($this->config['prefix'] . $key);
    }

    public function retrieve($key) {
        if(array_key_exists($key, $this->data))
            return $this->data[$key];
        
        $raw = $this->db->get($this->config['prefix'] . $key);
        
        if($raw === false)
            return null;
        
        $data = unserialize($raw);
        $this->data[$key] = $data;
        return $data;
    }

    public function store($key, $value, $duration = 60) {
        $this->db->setex($this->config['prefix'] . $key, $duration, serialize($value));
    }
}
