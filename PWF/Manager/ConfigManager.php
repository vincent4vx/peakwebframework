<?php

/*
 * The MIT License
 *
 * Copyright 2016 vincent.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace PWF\Manager;

use PWF\Exception\InvalidConfigException;

/**
 * Load and store configuration files
 *
 * @author vincent
 */
class ConfigManager {
    private static $configs = [];
    
    /**
     * Load a config file in PHP
     * @param string $file the config file name
     * @return array The configuration
     */
    static public function loadConfig($file){
        $file = realpath($file);
        
        if(isset(self::$configs[$file]))
            return self::$configs[$file];
        
        if(!is_file($file))
            throw new InvalidConfigException('Config file "' . $file . '" not found.');
        
        return (self::$configs[$file] = require $file);
    }
}
