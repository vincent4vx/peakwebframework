<?php

/*
 * The MIT License
 *
 * Copyright 2015 Vincent Quatrevieux <quatrevieux.vincent@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace PWF\Arch;

use PWF\Kernel;
use PWF\Output\Template\Template;

/**
 * Controller with templates
 *
 * @author Vincent Quatrevieux <quatrevieux.vincent@gmail.com>
 */
abstract class FrontEndController extends Controller{
    /**
     *
     * @var Template
     */
    protected $template;
    
    public function __construct(Kernel $kernel, Template $template) {
        parent::__construct($kernel);
        $this->template = $template;
    }
    
    /**
     * @see Template::render()
     */
    public function render($template, array $vars = []){
        return $this->template->render($template, $vars);
    }
    
    public function setLayout($layout){
        $this->template->setLayout($layout);
    }
    
    public function setTitle($title){
        $this->template->setTitle($title);
    }
    
    public function setLayoutVar($var, $value){
        $this->template->setLayoutVar($var, $value);
    }
}
