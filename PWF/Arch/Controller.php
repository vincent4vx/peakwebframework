<?php

/*
 * The MIT License
 *
 * Copyright 2015 Vincent Quatrevieux <quatrevieux.vincent@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace PWF\Arch;

use PWF\Output\Output;
use PWF\Kernel;
use PWF\Input\HttpInput;
use PWF\Output\InternalOutput;

/**
 * 
 * @author Vincent Quatrevieux <quatrevieux.vincent@gmail.com>
 * 
 * @property-read \PWF\Input\HttpInput $input Only on WebApp
 * @property-read \PWF\Loader $loader The Loader
 * @property-read array $config Configuration
 */
class Controller implements Kernel{
    /**
     *
     * @var Output
     */
    protected $output;
    
    /**
     *
     * @var Kernel
     */
    private $kernel;
    
    public function __construct(Kernel $kernel) {
        $this->kernel = $kernel;
    }

    public function __get($name) {
        return $this->kernel->$name;
    }
    
    public function __set($name, $value) {
        $this->kernel->$name = $value;
    }
    
    public function __isset($name) {
        return isset($this->kernel->$name);
    }
    
    public function conf($key, $default = null) {
        return $this->kernel->conf($key, $default);
    }

    public function forceSet($name, $value) {
        $this->kernel->forceSet($name, $value);
    }

    public function run($controller, $action, array $args = array()) {
        return $this->kernel->run($controller, $action, $args);
    }
    
    public function setOutput(Output $output) {
        $this->output = $output;
    }
    
    public function execute(HttpInput $input = null, Output $output = null) {
        if($output === null)
            $output = new InternalOutput();
                
        return $this->kernel->execute($input, $output);
    }
}
