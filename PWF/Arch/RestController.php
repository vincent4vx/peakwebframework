<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace PWF\Arch;

use PWF\Helper\Strings;
use PWF\Router\RestRouter;
use ReflectionClass;
use ReflectionMethod;

/**
 * Controller for REST application
 * @author vincent
 */
abstract class RestController extends HmvcController{
    /**
     * Get all the HTTP methods, actions with parameters for the current resource
     * [description] => The description of the current resource
     * [methods] => list of available HTTP methods
     * [actions] => list of available actions
     * 
     * For [methods] and [actions] lists :
     * [description] => Description of the method / action
     * [result] => The type of the result
     * [params] => list of path parameters with type, name and description
     * [path] => The path to access to this action / method
     * @return array
     */
    public function httpOPTIONS(){
        $routerConf = $this->loader->getInstance(RestRouter::class)->getConfig($this->input);
        
        $class = new ReflectionClass($this);
        $classComment = $this->helpers->PhpDocParser->parse($class);
        
        $infos = [
            'description' => $classComment->getDescription(),
            'methods' => [],
            'actions' => []
        ];
        
        foreach($class->getMethods(ReflectionMethod::IS_PUBLIC) as $method){
            $doc = $this->helpers->PhpDocParser->parse($method);
            
            $data = [
                'description' => $doc->getDescription()
            ];
            
            if(isset($doc->return)){
                $data['result'] = $doc->return[0]->getType();
            }
            
            $basePath = strtolower($class->getShortName()) . '/';
            $pathParams = [];
            
            foreach($method->getParameters() as $param){
                $pathParams[] = '[$' . $param->name . ']';
            }
            
            if(!empty($doc->param)){
                $data['params'] = [];
                
                foreach($doc->param as $param){
                    $data['params'][$param->getName()] = [
                        'type' => $param->getType(),
                        'description' => $param->getDescription()
                    ];
                }
            }
            
            $methodName = $method->name;
            
            if(Strings::startWith($methodName, $routerConf['http_method_prefix'])){
                $data['path'] = $basePath . implode('/', $pathParams);
                $infos['methods'][substr($methodName, strlen($routerConf['http_method_prefix']))] = $data;
            }elseif(Strings::endWith($methodName, $routerConf['action_suffix'])){
                $actionName = substr($methodName, 0, -strlen($routerConf['action_suffix']));
                $data['path'] = $basePath . $actionName . '/' . implode('/', $pathParams);
                $infos['actions'][$actionName] = $data;
            }
        }
        
        return $infos;
    }
}
