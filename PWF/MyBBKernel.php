<?php   
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace PWF;

use datacache;
use DB_MySQL;
use DB_MySQLi;
use DB_PgSQL;
use DB_SQLite;
use errorHandler;
use MyBB;
use MyLanguage;
use pluginSystem;
use PWF\MyBB\Middleware\MyBBOutputPage;
use PWF\MyBB\Middleware\MyBBOutputPrepare;
use PWF\MyBB\Middleware\MyBBStartUpMiddleware;
use session;
use templates;
use timer;

/**
 * Kernel for apps integreted into MyBB
 *
 * @author vincent
 * 
 * @property MyBB $mybb MyBB instance
 * @property errorHandler $mybb_error_handler MyBB error handler
 * @property timer $mybb_maintimer MyBB maintimer
 * @property array $mybb_config MyBB config array
 * @property templates $mybb_templates MyBB templates system
 * @property DB_MySQL|DB_MySQLi|DB_PgSQL|DB_SQLite $mybb_db MyBB database system
 * @property pluginSystem $mybb_plugins MyBB plugins system
 * @property datacache $mybb_cache MyBB cache system
 * @property MyLanguage $mybb_lang MyBB i18n system
 * @property array $mybb_settings MyBB settings array
 * @property string $mybb_version MyBB version string
 * @property array $mybb_date_formats
 * @property array $mybb_time_formats
 * @property session $mybb_session MyBB session system
 */
class MyBBKernel extends WebKernel{
    public function __construct(Loader $loader, array $config) {
        parent::__construct($loader, $config);
    }
    
    protected function registerMiddlwares() {
        parent::registerMiddlwares();
        
        $this->middlewares->startup($this->loader->getInstance(MyBBStartUpMiddleware::class));
        
        $this->middlewares->before($this->loader->getInstance(MyBBOutputPrepare::class));
        $this->middlewares->after($this->loader->getInstance(MyBBOutputPage::class));
    }
}
