<?php

/*
 * The MIT License
 *
 * Copyright 2015 Vincent Quatrevieux <quatrevieux.vincent@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace PWF;

use PWF\Exception\InvalidClassConstructorException;
use PWF\Interfaces\AfterConstruct;

require_once 'Constants.php';

/**
 * Load and store instances
 *
 * @author Vincent Quatrevieux <quatrevieux.vincent@gmail.com>
 */
class Loader {
    private $instances = [];
    private $modules = [];
    private $factories = [];
    
    static private $autoloaderInit = false;
    static private $autoloaderPaths = [];
    
    public function __construct() {
        $this->setInstance($this);
    }

    /**
     * Set a new instance into the Loader
     * @param mixed $obj
     */
    public function setInstance($obj){
        $this->instances[self::normalizeClassName(get_class($obj))] = $obj;
    }
    
    static private function normalizeClassName($className){
        if($className{0} === '\\')
            return substr($className, 1);
        
        return $className;
    }
    
    private function getImplementationClass($className){
        $className = self::normalizeClassName($className);
        
        if(isset($this->modules[$className]))
            return $this->modules[$className];
        
        return $className;
    }
    
    /**
     * Set a custom factory
     * @param string $className The class name
     * @param callable $factory The factory, take Loader as first parameter and returns The new instance
     * <code>
     * registerFactory(MyClass::class, function(Loader $loader) use($arg1, $arg2){
     *     return new MyClass($arg1, $arg2);
     * });
     * </code>
     */
    public function registerFactory($className, callable $factory){
        $this->factories[self::normalizeClassName($className)] = $factory;
    }
    
    /**
     * Retreive or create an instance of class name
     * @param string $className the class name
     * @return object
     */
    public function getInstance($className){
        $className = $this->getImplementationClass($className);
        
        //FIX getInstance for Loader::class
        if($className === Loader::class)
            return $this;
        
        if(isset($this->instances[$className]))
            return $this->instances[$className];
        
        $obj = $this->newInstance($className);
        $this->instances[$className] = $obj;
        
        if($obj instanceof AfterConstruct)
            $obj->afterConstruct($this);
        
        return $obj;
    }
    
    /**
     * Create a new instance of $className
     * @param string $className The class name
     * @return object the new instance
     * @throws InvalidClassConstructorException when the Loader can't found a parameter class
     */
    public function newInstance($className, array $parameters = []) {
        $className = $this->getImplementationClass($className);
        
        if(isset($this->factories[$className]))
            return $this->factories[$className]($this);
        
        $class = new \ReflectionClass($this->getImplementationClass($className));
        
        if($class->isInterface()
                || $class->isAbstract())
            throw new \InvalidArgumentException('The type ' . $className . ' is abstract or an interface. You should define a module (i.e. Loader::setModule()) before using-it');
        
        $constructor = $class->getConstructor();

        if($constructor === null) //no contructor, use default contructor
            return $class->newInstanceWithoutConstructor();

        $args = array();

        foreach($constructor->getParameters() as $param) {
            $paramClass = $param->getClass();

            if($paramClass === null)
                throw new InvalidClassConstructorException($class->getName(), $param->getName());
            
            $obj = null;
            
            foreach($parameters as $p){
                if($paramClass->isInstance($p)){
                    $obj = $p;
                    break;
                }
            }

            if($obj === null)
                $obj = $this->getInstance($paramClass->getName());
            
            $args[] = $obj;
        }

        return $class->newInstanceArgs($args);
    }
    
    /**
     * Define a implementation class ($implClass) for an interface class ($intClass)
     * @param string $intClass Interface class name
     * @param string $implClass Implementation class name
     */
    public function setModule($intClass, $implClass){
        $intClass = $this->normalizeClassName($intClass);
        $implClass = $this->normalizeClassName($implClass);
        $this->modules[$intClass] = $implClass;
    }
    
    /**
     * Define modules
     * @param array $modules [intClass] => [implClass]
     */
    public function setModules(array $modules){
        foreach($modules as $intClass => $implClass) {
            $this->setModule($intClass, $implClass);
        }
    }
    
    static public function initAutoloader(){
        if(self::$autoloaderInit)
            return;
        
        self::$autoloaderPaths[] = Constants::ROOT_DIR;
        
        spl_autoload_register(function($className){
            $className = self::normalizeClassName($className);
            
            foreach(self::$autoloaderPaths as $path) {
                $filename = $path . str_replace('\\', DIRECTORY_SEPARATOR, $className) . Constants::EXT;
                
                if(!is_file($filename))
                    continue;
                
                require $filename;
                break;
            }
        });
    }
    
    /**
     * Add a new path for the default autoloader
     * @param string $path The root path. Should end with /
     */
    static public function addAutoloaderPath($path){
        if($path{strlen($path) - 1} !== '/')
            $path .= '/';
        
        self::$autoloaderPaths[] = $path;
    }
}

Loader::initAutoloader();
