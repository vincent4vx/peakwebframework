<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace PWF\Debug;

use Exception;
use PWF\Debug\ErrorFormater\DefaultErrorFormater;
use PWF\Debug\ErrorFormater\ErrorFormater;
use PWF\Debug\ErrorFormater\JsonErrorFormater;
use PWF\Debug\ErrorFormater\PlainTextErrorFormater;
use PWF\Exception\InvalidConfigException;
use PWF\Executor\Executor;
use PWF\Kernel;
use PWF\Output\HttpException;
use PWF\Output\HttpRedirect;
use PWF\Output\Output;
use PWF\Router\Route;

/**
 * Handle for Http exceptions
 * @author vincent
 */
class HttpExceptionHandler implements ExceptionHandler{
    private $config;
    /**
     * @var Executor
     */
    private $executor;
    
    /**
     * @var Kernel
     */
    private $kernel;
    
    public function __construct(Kernel $kernel, Executor $executor) {
        $this->config = $kernel->conf('errors.http', $this->getDefaultConfig());
        $this->executor = $executor;
        $this->kernel = $kernel;
    }
    
    protected function getDefaultConfig(){
        return [
            'actions' => [],
            'formaters' => [
                Output::MIME_HTML => new DefaultErrorFormater('httperror.html.php'),
                Output::MIME_JSON => new JsonErrorFormater(),
                'default' => new PlainTextErrorFormater()
            ]
        ];
    }

    public function handle(Exception $e, Output $output) {
        if(!($e instanceof HttpException))
            return false;
        
        $output->setCode($e->getCode());
        
        if($e instanceof HttpRedirect)
            $this->redirect($e, $output);
        
        if(isset($this->config['actions'][$e->getCode()])){
            $this->handleCustom($this->config['actions'][$e->getCode()], $output, $e);
        }else{
            $this->handleDefault($output, $e);
        }
        
        return true;
    }
    
    protected function handleCustom($action, Output $output, HttpException $e){
        if($action instanceof Route){
            $ret = $this->executor->route($action);
        }elseif(is_callable($action)){
            $ret = call_user_func_array($action, [$output, $e]);
        }else{
            throw new InvalidConfigException('Bad config value for errors.http.actions.' . $e->getCode());
        }
            
        if($ret !== null)
            $output->setBody($ret);
    }
    
    protected function handleDefault(Output $output, HttpException $e){
        $formater = isset($this->config['formaters'][$output->getMimeType()])
                ? $this->config['formaters'][$output->getMimeType()] : $this->config['formaters']['default'];
        
        if(!($formater instanceof ErrorFormater))
            throw new InvalidConfigException('errors.http.formaters need an instance of ErrorFormater');
        
        $ret = $formater->formatError($output, $e, $this->kernel);
        
        if($ret !== null)
            $output->setBody($ret);
    }

    protected function redirect(HttpRedirect $r, Output $output){
        $output->setHeader('Location', $r->getLocation());
    }
}
