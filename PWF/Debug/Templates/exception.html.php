<section class="debug error">
    <?php if($error instanceof \ErrorException):?>
    <h1><?= $this->getSeverity($error->getSeverity())?></h1>
    <?php else:?>
    <h1>Exception : <?= (new ReflectionClass($error))->getShortName()?></h1>
    <?php endif?>
    <article>
        <h3>Message :</h3>
        <p class="debug info"><?= $error->getMessage()?></p>
    </article>

    <article>
        <h3>Debug :</h3>
        <div class="debug info">
            <p>On file <em><?= $error->getFile()?></em> line <strong><?= $error->getLine()?></strong> :</p>
            <?php $this->showErrorFile($error->getFile(), $error->getLine())?>
        </div>
    </article>

    <?php if(!empty($error->getTrace())):?>
    <article>
        <h3>Stack trace :</h3>
        <div class="debug info">
            <?php $this->showTrace($error->getTrace())?>
        </div>
    </article>
    <?php endif?>
    
    <?php if($error instanceof PWF\Debug\Exception\ExtendedErrorException && !empty($error->getContext())):?>
    <article>
        <h3>Context :</h3>
        <div class="debug info">
            <?php $this->showContext($error->getContext())?>
        </div>
    </article>
    <?php endif?>
</section>