<!DOCTYPE html>
<html>
    <head>
        <title>Error</title>
        <meta charset="utf-8"/>
        <meta name="robots" content="noindex,nofollow"/>
        <style><?= $this->getErrorCSS()?></style>
    </head>
    <body>
        <div id="content"><?= $contents?></div>
        <footer>
            Powered by <strong><a href="https://bitbucket.org/vincent4vx/peakwebframework" target="_blank">PeakWebFramework</a></strong>
        </footer>
    </body>
</html>