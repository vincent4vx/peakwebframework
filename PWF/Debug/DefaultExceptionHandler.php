<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace PWF\Debug;

use Exception;
use PWF\Debug\ErrorFormater\DefaultErrorFormater;
use PWF\Debug\ErrorFormater\ErrorFormater;
use PWF\Debug\ErrorFormater\JsonErrorFormater;
use PWF\Debug\ErrorFormater\PlainTextErrorFormater;
use PWF\Exception\InvalidConfigException;
use PWF\Kernel;
use PWF\Output\Output;

/**
 * Handler for all exceptions
 * @author vincent
 */
class DefaultExceptionHandler implements ExceptionHandler{
    /**
     * @var Kernel
     */
    private $kernel;
    
    private $formats;
    
    public function __construct(Kernel $kernel) {
        $this->kernel = $kernel;
        $this->formats = $this->kernel->conf('errors.default', $this->getDefaultFormats());
    }

    protected function getDefaultFormats(){
        return [
            Output::MIME_HTML => new DefaultErrorFormater(),
            Output::MIME_JSON => new JsonErrorFormater(),
            'default' => new PlainTextErrorFormater(),
        ];
    }

    public function handle(Exception $e, Output $output) {
        $formater = isset($this->formats[$output->getMimeType()]) ?
            $this->formats[$output->getMimeType()] : $this->formats['default'];
        
        if(!($formater instanceof ErrorFormater))
            throw new InvalidConfigException('errors.default need instances of ErrorFormater');
        
        $ret = $formater->formatError($output, $e, $this->kernel);
        
        if($ret !== null)
            $output->setBody($ret);
    }
}
