<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace PWF\Debug;

use PWF\Output\Output;

/**
 * Handler for Exceptions
 * @author vincent
 */
interface ExceptionHandler {
    /**
     * Handle the exception
     * @param Exception $e
     * @param Output $output The output
     * @return bool true if the Exception is handled, or false in other cases
     */
    public function handle(\Exception $e, Output $output);
}
