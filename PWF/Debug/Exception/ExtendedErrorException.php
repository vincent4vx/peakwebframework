<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace PWF\Debug\Exception;

/**
 * 
 * @author vincent
 */
class ExtendedErrorException extends \ErrorException{
    private $context;
    
    public function __construct($message, $severity, $filename, $lineno, array $context) {
        parent::__construct($message, 0, $severity, $filename, $lineno);
        $this->context = $context;
    }
    
    public function getContext() {
        return $this->context;
    }

    static public function createFromError(array $error){
        return new ExtendedErrorException(
            $error['message'], 
            $error['type'], 
            $error['file'], 
            $error['line'], 
            isset($error['context']) ? $error['context'] : []
        );
    }
}
