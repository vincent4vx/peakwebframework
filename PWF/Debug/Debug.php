<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace PWF\Debug;

/**
 * Debug utils
 * @author vincent
 */
class Debug {
    /**
     * Show a php code, with highlighted line
     * @param string $code Code to show
     * @param int $errorLine The line to highlight
     */
    public function showCode($code, $errorLine){
        $openTag = strpos($code, '<?');
        $closeTag = false;
        $openTagAdded = false;
        
        if($openTag !== false){
            $closeTag = strpos($code, '?>');
        }
        
        //There is not open tag or there is a close tag before the open (i.e. The open tag is missing)
        if($openTag === false
            || ($closeTag !== false && $closeTag < $openTag)){
            $code = '<?php' . PHP_EOL . $code; //Add the open tag
            $openTagAdded = true;
        }
        
        $str = highlight_string($code, true);
        
        $str = preg_replace('#' . $errorLine . '\..*<br\s*/?>#iU', '<strong style="background: rgba(0,0,0,.1);">$0</strong>', $str);
        
        if($openTagAdded)
            $str = str_replace('&lt;?php<br />', '', $str);
        
        echo $str;
    }
    
    public function showErrorFile($errorFile, $errorLine){
        if(strpos($errorFile, "eval()'d code") !== false){
            return $this->showEvalCode($errorFile, $errorLine);
        }
        
        $code = '';
        $file = file($errorFile);
        $lineStart = $errorLine - 3;
        
        if($lineStart < 0)
            $lineStart = 0;
        
        for($line = $lineStart; $line < $errorLine + 2 && $line < count($file); ++$line){
            $code .= ($line + 1) . '. ' . $file[$line];
        }
        
        $this->showCode($code, $errorLine);
    }
    
    public function showEvalCode($errorFile, $errorLine){
        $matches = [];
        
        if(!preg_match("#(.*)\((\d+)\) : eval\(\)'d#iU", $errorFile, $matches)) //not match ? 
            return;
        
        $realFile = $matches[1];
        $realLine = $matches[2];
        
        $this->showErrorFile($realFile, $realLine);
    }
    
    public function showTrace(array $trace){
        echo '<table class="debug trace">';
        
        echo '<tr><th>#</th><th>File</th><th>Function</th></tr>';
        
        foreach($trace as $i => $line){
            echo '<tr>';
            
            echo "<td>#{$i}</td>";
            
            if(isset($line['file'], $line['line'])){
                echo '<td class="debug file">', htmlentities($line['file']), ':', $line['line'];

                echo '<div class="debug code">', $this->showErrorFile($line['file'], $line['line']), '</div>';

                echo '</td>';
            }else{
                echo '<td>[PHP]</td>';
            }
            
            echo '<td class="debug function">';
            
            if(isset($line['class'], $line['type']))
                echo htmlentities($line['class']), $line['type'];
            
            echo $line['function'], '<span class="debug args">(';
                    
            if(isset($line['args']))
                echo $this->formatArgs($line['args']);
            
            echo ')</span></td>';
            
            echo '</tr>';
        }
        
        echo '</table>';
    }
    
    public function formatArgs(array $args){
        $params = [];
        foreach($args as $k => $arg){
            $val = '';
            switch(gettype($arg)){
                case 'boolean':
                    $val = '<em>bool</em>(<span class="debug arg-bool">' . ($arg ? 'true' : 'false') . '</span>)';
                    break;
                case 'string':
                    $val = '<em>string</em>(<span class="debug arg-string">"' . htmlentities($arg) . '"</span>)';
                    break;
                case 'integer':
                    $val = '<em>int</em>(<span class="debug arg-int">' . $arg . '</span>)';
                    break;
                case 'float':
                case 'double':
                    $val = '<em>float</em>(<span class="debug arg-float">' . $arg . '</span>)';
                    break;
                case 'object':
                    $val = '<em>object</em>(<span class="debug arg-object">' . get_class($arg) . '</span>)';
                    break;
                case 'array':
                    $val = '<em>array</em>{' . $this->formatArgs($arg) . '}';
                    break;
                case 'NULL':
                    $val = '<span class="debug arg-null">NULL</span>';
                    break;
                default:
                    $val = var_export($arg, true);
            }
            
            $params[] = is_int($k) ? $val : '"' . htmlentities($k) . '" => ' . $val; 
        }
        
        return implode(', ', $params);
    }
    
    public function showException(\Exception $error, $template = 'exception.html.php'){
        require $this->templatesDir() . $template;
    }
    
    public function decorateError(\Exception $error, $contents){
        require $this->templatesDir() . 'layout.html.php';
    }
    
    public function showExceptionPage(\Exception $error, $template = 'exception.html.php'){
        ob_start();
        $this->showException($error, $template);
        $this->decorateError($error, ob_get_clean());
    }
    
    public function getErrorCSS(){
        return file_get_contents($this->templatesDir() . 'error.css');
    }
    
    public function templatesDir(){
        return __DIR__ . DIRECTORY_SEPARATOR . 'Templates' . DIRECTORY_SEPARATOR;
    }
    
    public function showContext(array $context){
        echo <<<'EOD'
    <table class="debug context">
        <tr>
            <th>Var name</th><th>Value</th>
        </tr>
EOD;
        
        foreach($context as $name => $value){
            echo '<tr><td>', $name, '</td><td>';
            var_dump($value);
            echo '</td></tr>';
        }
            
        echo '</table>';
    }
    
    public function getSeverity($code){
        switch($code){
            case E_ERROR:
            case E_CORE_ERROR:
            case E_USER_ERROR:
                return 'Fatal error';
            case E_COMPILE_ERROR:
                return 'Compile error';
            case E_USER_WARNING:
            case E_CORE_WARNING:
            case E_COMPILE_WARNING:
            case E_WARNING:
                return 'Warning';
            case E_NOTICE:
            case E_USER_NOTICE:
                return 'Notice';
            case E_DEPRECATED:
            case E_USER_DEPRECATED:
                return 'Deprecated';
            case E_STRICT:
                return 'Strict standards';
            case E_PARSE:
                return 'Parse error';
            case E_RECOVERABLE_ERROR:
                return 'Catchable error';
            default:
                return 'Undefined error';
        }
    }
}
