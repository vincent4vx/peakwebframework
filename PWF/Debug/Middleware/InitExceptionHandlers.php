<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace PWF\Debug\Middleware;

use PWF\Debug\DefaultExceptionHandler;
use PWF\Debug\ErrorHandler;
use PWF\Debug\HttpExceptionHandler;
use PWF\Executor\Middleware\Middleware;
use PWF\Input\HttpInput;
use PWF\Kernel;
use PWF\Output\Output;

/**
 * Init exception handlers
 * @author vincent
 */
class InitExceptionHandlers implements Middleware{
    /**
     * @var ErrorHandler
     */
    private $errorHandler;
    
    /**
     * @var Kernel
     */
    private $kernel;
    
    public function __construct(ErrorHandler $errorHandler, Kernel $kernel) {
        $this->errorHandler = $errorHandler;
        $this->kernel = $kernel;
    }

    public function priority() {
        return 10;
    }

    public function run(HttpInput $input = null, Output $output = null) {
        foreach($this->kernel->conf('errors.handlers', [HttpExceptionHandler::class, DefaultExceptionHandler::class]) as $handler) {
            $this->errorHandler->registerExceptionHandler($this->kernel->loader->getInstance($handler));
        }
    }
}
