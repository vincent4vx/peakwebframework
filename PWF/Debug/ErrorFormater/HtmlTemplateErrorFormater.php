<?php

/*
 * The MIT License
 *
 * Copyright 2015 Vincent Quatrevieux <quatrevieux.vincent@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace PWF\Debug\ErrorFormater;

use PWF\Kernel;
use PWF\Output\Output;
use PWF\Output\Template\Template;

/**
 * ErrorFormater using Template system
 * Don't forget to set this class into error_format config and create templates files
 *
 * @author Vincent Quatrevieux <quatrevieux.vincent@gmail.com>
 */
class HtmlTemplateErrorFormater implements ErrorFormater{
    private $template;
    
    public function __construct($template) {
        $this->template = $template;
    }
    
    public function formatError(Output $output, \Exception $error, Kernel $kernel) {
        $template = $kernel->loader->getInstance(Template::class);
        
        echo $template->render($this->template, ['error' => $error]);
    }
}
