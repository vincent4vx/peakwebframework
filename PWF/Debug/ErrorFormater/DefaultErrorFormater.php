<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace PWF\Debug\ErrorFormater;

use PWF\Debug\Debug;
use PWF\Kernel;
use PWF\Output\Output;

/**
 * Error formater using Debug class
 *
 * @author vincent
 */
class DefaultErrorFormater implements ErrorFormater{
    private $template;
    
    public function __construct($template = 'exception.html.php') {
        $this->template = $template;
    }

    public function formatError(Output $output, \Exception $error, Kernel $kernel) {
        $debug = $kernel->loader->getInstance(Debug::class);
        ob_start();
        $debug->showExceptionPage($error, $this->template);
        $output->setBody(ob_get_clean());
    }
}
