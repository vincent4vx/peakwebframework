<?php

/*
 * The MIT License
 *
 * Copyright 2016 vincent.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace PWF\Debug;

use PWF\Constants;
use PWF\Debug\Exception\ExtendedErrorException;
use PWF\Debug\Middleware\InitExceptionHandlers;
use PWF\Executor\Middleware\Middlewares;
use PWF\Interfaces\AfterConstruct;
use PWF\Loader;
use PWF\Output\HttpException;
use PWF\Output\Output;

/**
 * Handle errors
 * @author vincent
 */
class ErrorHandler implements AfterConstruct{
    /**
     * @var Output
     */
    private $output;
    
    private $showErrors;
    
    /**
     * @var ExceptionHandler[]
     */
    private $handlers = [];
    
    /**
     * @var Debug
     */
    private $debug;

    public function __construct(Output $output = null, Debug $debug = null) {
        $this->output = $output;
        $this->debug = $debug ?: new Debug();
        $this->showErrors = Constants::DEBUG;
        
        register_shutdown_function(function(){
            $error = error_get_last();
            
            if(!$error || !$this->isFatal($error)){
                return;
            }
            
            while(ob_get_level() > 0)
                ob_end_clean();
            
            $this->handleException(ExtendedErrorException::createFromError($error));
        });
        
        set_error_handler([$this, 'handleError']);
        set_exception_handler([$this, 'handleException']);
    }
    
    public function afterConstruct(Loader $loader) {
        $loader->getInstance(Middlewares::class)->startup(
            $loader->getInstance(InitExceptionHandlers::class)
        );
    }

    public function getShowErrors() {
        return $this->showErrors;
    }

    public function setShowErrors($showErrors) {
        $this->showErrors = $showErrors;
    }
    
    public function handleException(\Exception $e, Output $output = null){
        if($output === null)
            $output = $this->output;
        
        $output->setError($e);
        
        if(!$this->showErrors && !($e instanceof HttpException)){
            $e = new HttpException('Internal server error', Output::CODE_INTERNAL_ERROR);
        }
        
        ob_start();
        
        try{
            foreach($this->handlers as $handler) {
                if($handler->handle($e, $output) !== false)
                    return;
            }
        }catch(\Exception $ex){
            if($this->showErrors)
                $this->debug->showException($ex);
        }
        
        if($this->showErrors)
            $this->debug->showException($e);
        
        $contents = ob_get_clean();
        
        ob_start();
        $this->debug->decorateError($e, $contents);
        
        $output->setBody(ob_get_clean());
    }
    
    public function getTrace(){
        $trace = debug_backtrace();
        
        while(!empty($trace) && isset($trace[0]['class'])
                && $trace[0]['class'] === self::class)
            array_shift($trace);
        
        return $trace;
    }
    
    public function handleError($errno, $errstr, $errfile, $errline, $errcontext){
        if(($errno & error_reporting()) !== $errno)
            return;
        
        throw new ExtendedErrorException(
            $errstr,
            $errno,
            $errfile,
            $errline,
            $errcontext
        );
    }
    
    private function isFatal(array $error){
        return in_array($error['type'], [E_ERROR, E_COMPILE_ERROR, E_CORE_ERROR, E_USER_ERROR, E_PARSE]);
    }
    
    public function registerExceptionHandler(ExceptionHandler $handler, $prepend = false){
        if($prepend)
            array_unshift($this->handlers, $handler);
        else
            $this->handlers[] = $handler;
    }
}