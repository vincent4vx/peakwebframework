<?php

/*
 * The MIT License
 *
 * Copyright 2016 vincent.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace PWF\Executor;

use PWF\Arch\Controller;
use PWF\Input\HttpInput;
use PWF\Output\HttpNotFound;
use PWF\Output\Output;
use PWF\Router\Route;

/**
 * Executor handle page execution process
 * @author vincent
 */
interface Executor {
    /**
     * Execute a controller
     * @param Controller $controller The controller object
     * @param string $actionName The action name
     * @param array $parameters The parameters of the action, as array
     * @return mixed The action result
     * @throws HttpNotFound When $actionName is not a method of $controller
     */
    public function controller(Controller $controller, $actionName, array $parameters);
    
    /**
     * Execute a route. Use Executor::Controller()
     * To instanciate the controller, use Input and Output instance from Input / Output Stacks.
     * @param Route $route The route instance
     * @return mixed The action result
     * @throws HttpNotFound When controller is not found, or invalid action name
     * @see Executor::controller($controller, $actionName, $parameters)
     */
    public function route(Route $route);
    
    /**
     * Execute the application :
     * - Add $input and $output to stacks
     * - Execute BEFORE middlwares
     * - Get the route
     * - Execute the route
     * - Execute AFTER middlwares
     * - return the output
     * - Remove from stack $input and $output
     * @param HttpInput $input
     * @param Output $output
     * @return Output The output instance
     */
    public function app(HttpInput $input = null, Output $output = null);
}
