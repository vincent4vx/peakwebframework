<?php

/*
 * The MIT License
 *
 * Copyright 2016 vincent.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace PWF\Executor;

use InvalidArgumentException;
use PWF\Arch\Controller;
use PWF\Debug\ErrorHandler;
use PWF\Executor\Middleware\Middlewares;
use PWF\Executor\Middleware\MiddlewaresStack;
use PWF\Input\HttpInput;
use PWF\Input\InputStack;
use PWF\Loader;
use PWF\Output\HttpNotFound;
use PWF\Output\Output;
use PWF\Output\OutputStack;
use PWF\Router\Route;
use PWF\Router\Router;
use ReflectionClass;

/**
 * @author vincent
 */
class ExecutorImpl implements Executor{
    /**
     * @var Loader
     */
    private $loader;
    
    /**
     * @var MiddlewaresStack
     */
    private $middlewares;
    
    /**
     * @var InputStack
     */
    private $inputStack;
    
    /**
     * @var OutputStack
     */
    private $outputStack;
    
    /**
     * @var ErrorHandler
     */
    private $errorHandler;
    
    /**
     * @var Router
     */
    private $router;
    
    public function __construct(Loader $loader, MiddlewaresStack $middlewares, InputStack $inputStack, OutputStack $outputStack, ErrorHandler $errorHandler, Router $router) {
        $this->loader = $loader;
        $this->middlewares = $middlewares;
        $this->inputStack = $inputStack;
        $this->outputStack = $outputStack;
        $this->errorHandler = $errorHandler;
        $this->router = $router;
    }

    public function app(HttpInput $input = null, Output $output = null) {
        if($input !== null)
            $this->inputStack->push($input);
        
        if($output !== null)
            $this->outputStack->push($output);
        
        //Create a new Middlewares handler
        $this->middlewares->fork();
        
        try{
            $curOutput = $this->outputStack->current();
            
            $route = $this->router->getRoute($this->inputStack->current());
            
            if(!class_exists($route->controller()))
                throw new HttpNotFound('Controller not found');
            
            //Load route middlewares
            $this->middlewares->buildFromConfig($this->loader, $route->middlewares());
            
            //Preload dependencies in case of depency add a middleware
            $this->preloadCtrlDependencies($route->controller());
            
            $this->middlewares->run(Middlewares::EVENT_BEFORE);
            
            $res = $this->route($route);

            if($res !== null)
                $this->outputStack->current()->setBody($res);
            
            $this->middlewares->run(Middlewares::EVENT_AFTER);
        }catch(\Exception $ex) {
            $this->errorHandler->handleException($ex, $curOutput);
        }finally{
            if($input !== null)
                $this->inputStack->pop();
            
            if($output !== null)
                $this->outputStack->pop();
            
            $this->middlewares->pop();
        }
        
        return $curOutput;
    }

    public function controller(Controller $controller, $actionName, array $parameters) {
        if(!method_exists($controller, $actionName))
            throw new HttpNotFound('Invalid action');
        
        return call_user_func_array([$controller, $actionName], $parameters);
    }

    public function route(Route $route) {
        if(!class_exists($route->controller()))
            throw new HttpNotFound('Controller not found');
        
        $controller = $this->loader->newInstance($route->controller(), [
            $this->inputStack->current(),
            $this->outputStack->current()
        ]);
        
        if(!($controller instanceof Controller))
            throw new InvalidArgumentException(get_class($controller) . ' is not a valid instance of Controller');
        
        $controller->setOutput($this->outputStack->current());
        
        return $this->controller($controller, $route->action(), $route->parameters());
    }
    
    private function preloadCtrlDependencies($class){
        $ctrl = new ReflectionClass($class);
        
        if($ctrl->getConstructor() === null)
            return;
        
        foreach($ctrl->getConstructor()->getParameters() as $param){
            $this->loader->getInstance($param->getClass()->name);
        }
    }
}
