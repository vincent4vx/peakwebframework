<?php

/*
 * The MIT License
 *
 * Copyright 2016 vincent.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace PWF\Executor\Middleware;

use PWF\Exception\InvalidConfigException;
use PWF\Helper\Arrays;
use PWF\Input\InputStack;
use PWF\Loader;
use PWF\Output\OutputStack;

/**
 * Handle middlewares
 *
 * @author vincent
 */
class MiddlewaresImpl implements Middlewares{
    /**
     * @var InputStack
     */
    private $inputStack;
    
    /**
     * @var OutputStack
     */
    private $outputStack;
    
    /**
     * @var MiddlewareQueue[]
     */
    private $middlewares = [];
    
    static private $eventsConfig = [
        self::EVENT_STARTUP => false,
        self::EVENT_BEFORE => true,
        self::EVENT_AFTER => true,
        self::EVENT_SHUTDOWN => false
    ];
    
    public function __construct(InputStack $inputStack, OutputStack $outputStack) {
        $this->inputStack = $inputStack;
        $this->outputStack = $outputStack;
    }

    /**
     * Add a middleware
     * @param string $event The event. Can be Middleware::EVENT_ constant, or an array, with name index as string for the middleware nane, and io index as bool, is Input and Output should be passed to the Middleware
     * @param Middleware $middleware The middleware instance
     */
    public function add($event, Middleware $middleware){
        if(!isset($this->middlewares[$event]))
            $this->middlewares[$event] = new MiddlewareQueue;
        
        $this->middlewares[$event]->insert($middleware);
    }
    
    public function startup(Middleware $middleware){
        $this->add(self::EVENT_STARTUP, $middleware);
    }
    
    public function shutdown(Middleware $middleware){
        $this->add(self::EVENT_SHUTDOWN, $middleware);
    }
    
    public function before(Middleware $middleware){
        $this->add(self::EVENT_BEFORE, $middleware);
    }
    
    public function after(Middleware $middleware){
        $this->add(self::EVENT_AFTER, $middleware);
    }
    
    /**
     * Run middlewares
     * @param string $event The event. Can be Middleware::EVENT_ constant, or an array, with name index as string for the middleware nane, and io index as bool, is Input and Output should be passed to the Middleware
     */
    public function run($event){
        if(!isset($this->middlewares[$event]))
            return;
        
        foreach($this->middlewares[$event] as $middleware){
            if(isset(self::$eventsConfig[$event]) && self::$eventsConfig[$event])
                $middleware->run($this->inputStack->current(), $this->outputStack->current());
            else
                $middleware->run();
        }
    }
    
    /**
     * Build the middlewares from a config array
     * Config format :
     * <code>
     * $conf = [
     *      //The index should be a string (i.e. the event name)
     *      'event' => [
     *          //class name
     *          MyMiddleware::class,
     * 
     *          //callback
     *          function(HttpInput $input = null, Output $output = null){
     *              //...
     *          },
     * 
     *          //Middleware instance
     *          new MyOtherMiddleware(),
     *      ],
     *      'evt2' => [
     *          //...
     *      ],
     *      //You can also use a Middlewares::EVENT_* constant
     *      Middlewares::EVENT_BEFORE => [
     *          //...
     *      ]
     * ];
     * </code>
     * @param Loader $loader
     * @param array $config The middlewares config
     * @throws InvalidConfigException
     */
    public function buildFromConfig(Loader $loader, array $config){
        foreach($config as $event => $middlewares){
            if(!is_string($event))
                throw new InvalidConfigException('Middleware event should be a string');
            
            if(!is_array($middlewares))
                throw new InvalidConfigException('Middlewares config should be an array');
            
            foreach($middlewares as $middleware){
                if(is_callable($middleware)){
                    $this->add($event, new ClosureMiddleware($middleware));
                }elseif($middleware instanceof Middleware){
                    $this->add($event, $middleware);
                }elseif(is_string($middleware)){
                    $this->add($event, $loader->getInstance($middleware));
                }else{
                    throw new InvalidConfigException('Invalid Middleware : ' . $middleware);
                }
            }
        }
    }
    
    public function __clone() {
        $this->middlewares = Arrays::cloneArrayRecurs($this->middlewares);
    }
}
