<?php

/*
 * The MIT License
 *
 * Copyright 2016 vincent.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace PWF\Executor\Middleware;

use PWF\Loader;
use SplStack;

/**
 * Stack for Middlewares
 *
 * @author vincent
 */
class MiddlewaresStack implements Middlewares{
    /**
     * @var \SplStack
     */
    private $stack;
    
    public function __construct(Loader $loader) {
        $this->stack = new SplStack();
        $this->stack->push($loader->newInstance(MiddlewaresImpl::class));
    }

    public function add($event, Middleware $middleware) {
        $this->current()->add($event, $middleware);
    }

    public function after(Middleware $middleware) {
        $this->current()->after($middleware);
    }

    public function before(Middleware $middleware) {
        $this->current()->before($middleware);
    }

    public function buildFromConfig(Loader $loader, array $config) {
        return $this->current()->buildFromConfig($loader, $config);
    }

    public function run($event) {
        $this->current()->run($event);
    }

    public function shutdown(Middleware $middleware) {
        $this->current()->shutdown($middleware);
    }

    public function startup(Middleware $middleware) {
        $this->current()->startup($middleware);
    }

    /**
     * Get the current Middlewares instance
     * @return Middlewares
     */
    public function current(){
        return $this->stack->top();
    }
    
    /**
     * Add a new Middlewares to the stack
     * @param Middlewares $middlewares
     */
    public function push(Middlewares $middlewares){
        $this->stack->push($middlewares);
    }
    
    /**
     * Remove the top of the MiddlewaresStack
     * @throws \UnderflowException
     * @return Middlewares The last middleware
     */
    public function pop(){
        if($this->stack->top() === $this->stack->bottom())
            throw new \UnderflowException('Cannot remove the last element of the MiddlewaresStack');
        
        return $this->stack->pop();
    }
    
    public function fork(){
        $middlewares = clone $this->current();
        $this->push($middlewares);
        return $middlewares;
    }
}
