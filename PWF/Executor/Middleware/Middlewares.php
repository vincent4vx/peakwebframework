<?php

/*
 * The MIT License
 *
 * Copyright 2016 vincent.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace PWF\Executor\Middleware;

use PWF\Exception\InvalidConfigException;
use PWF\Loader;

/**
 * Middlewares interface
 * @author vincent
 */
interface Middlewares {
    /**
     * Executed when Kernel is created (at the end of the constructor)
     * Don't use Input nor Output
     */
    const EVENT_STARTUP  = 'startup';
    
    /**
     * Executed when Kernel is destroyed
     * Don't use Input nor Output
     */
    const EVENT_SHUTDOWN = 'shutdown';
    
    /**
     * Executed before the controllers execution
     */
    const EVENT_BEFORE = 'before';
    
    /**
     * Executed after the controllers execution
     */
    const EVENT_AFTER = 'after';
    
    /**
     * Add a middleware
     * @param string $event The event. Can be Middleware::EVENT_ constant, or an array, with name index as string for the middleware nane, and io index as bool, is Input and Output should be passed to the Middleware
     * @param Middleware $middleware The middleware instance
     */
    public function add($event, Middleware $middleware);
    
    /**
     * Add a startup Middleware
     * @param Middleware $middleware
     */
    public function startup(Middleware $middleware);
    
    /**
     * Add a shutdown Middleware
     * @param Middleware $middleware
     */
    public function shutdown(Middleware $middleware);
    
    /**
     * Add a before Middleware
     * @param Middleware $middleware
     */
    public function before(Middleware $middleware);
    
    /**
     * Add a after Middleware
     * @param Middleware $middleware
     */
    public function after(Middleware $middleware);
    
    /**
     * Run middlewares
     * @param string $event The event. Can be Middleware::EVENT_ constant, or an array, with name index as string for the middleware nane, and io index as bool, is Input and Output should be passed to the Middleware
     */
    public function run($event);
    
    /**
     * Build the middlewares from a config array
     * Config format :
     * <code>
     * $conf = [
     *      //The index should be a string (i.e. the event name)
     *      'event' => [
     *          //class name
     *          MyMiddleware::class,
     * 
     *          //callback
     *          function(HttpInput $input = null, Output $output = null){
     *              //...
     *          },
     * 
     *          //Middleware instance
     *          new MyOtherMiddleware(),
     *      ],
     *      'evt2' => [
     *          //...
     *      ],
     *      //You can also use a Middlewares::EVENT_* constant
     *      Middlewares::EVENT_BEFORE => [
     *          //...
     *      ]
     * ];
     * </code>
     * @param Loader $loader
     * @param array $config The middlewares config
     * @throws InvalidConfigException
     */
    public function buildFromConfig(Loader $loader, array $config);
}
