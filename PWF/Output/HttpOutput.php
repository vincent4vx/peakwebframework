<?php

/*
 * The MIT License
 *
 * Copyright 2015 Vincent Quatrevieux <quatrevieux.vincent@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace PWF\Output;

use Exception;
use PWF\Exception\InvalidOperationException;
use PWF\Output\OutputFormater\OutputFormater;

/**
 * Output on front controllers for web page
 *
 * @author Vincent Quatrevieux <quatrevieux.vincent@gmail.com>
 */
class HttpOutput implements Output{
    use Util\OutputBufferTrait;
    
    /**
     * @var OutputFormater
     */
    private $outputFormater;
    
    private $mime = Output::MIME_HTML;
    
    private $error = null;
    
    public function __construct(OutputFormater $outputFormater) {
        $this->outputFormater = $outputFormater;
    }

    public function removeHeader($header) {
        header_remove($header);
    }

    public function setBody($body) {
        if($this->isBuffered())
            $this->resetBuffer();
        
        echo $this->outputFormater->formatOutput($this, $body);
    }

    public function setCode($code) {
        http_response_code((int)$code);
    }

    public function setHeader($header, $value) {
        header($header . ': ' .$value);
    }

    public function setMimeType($mime) {
        $this->mime = $mime;
        $this->setHeader('Content-Type', $mime . '; charset=UTF-8');
    }
    
    public function getMimeType() {
        return $this->mime;
    }
    
    public function setError(Exception $error) {
        $this->error = $error;
    }
    
    public function hasError() {
        return $this->error !== null;
    }
    
    public function getBody() {
        if($this->isBuffered())
            return $this->getBuffer();
        
        throw new InvalidOperationException('Cannot get the body on ' . __CLASS__ . ' when buffering is not enabled');
    }

    public function getCode() {
        return http_response_code();
    }

    public function getError() {
        return $this->error;
    }

    public function getHeader($header) {
        throw new InvalidOperationException('Cannot get header on ' . __CLASS__);
    }
}
