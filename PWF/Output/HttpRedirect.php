<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace PWF\Output;

/**
 * Description of HttpRedirect
 *
 * @author vincent
 */
class HttpRedirect extends HttpException{
    private $location;
    
    public function __construct($location, $code = Output::CODE_MOVE_TEMP) {
        $this->location = $location;
        parent::__construct('Redirected', $code);
    }
    
    public function getLocation() {
        return $this->location;
    }
}
