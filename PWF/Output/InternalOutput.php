<?php

/*
 * The MIT License
 *
 * Copyright 2015 Vincent Quatrevieux <quatrevieux.vincent@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace PWF\Output;

use Exception;
use PWF\Output\Util\OutputUtils;

/**
 * Output for internal requests
 *
 * @author Vincent Quatrevieux <quatrevieux.vincent@gmail.com>
 */
class InternalOutput implements Output{
    use Util\OutputBufferTrait;
    
    private $headers = [];
    private $body = null;
    private $code = Output::CODE_OK;
    private $mime = Output::MIME_INTERNAL;
    
    private $propagateErrors;
    
    public function __construct($propagateErrors = false) {
        $this->propagateErrors = $propagateErrors;
    }

    public function removeHeader($header) {
        unset($this->headers[$this->normalizeHeader($header)]);
    }

    public function setBody($body) {
        if($this->isBuffered){
            $this->resetBuffer();
            echo $body;
        }else{
            $this->body = $body;
        }
    }

    public function setCode($code) {
        $this->code = (int)$code;
    }

    public function setHeader($header, $value) {
        $this->headers[$this->normalizeHeader($header)] = $value;
    }
    
    private function normalizeHeader($header){
        $words = explode('-', $header);
        array_walk($words, function(&$value){
            $value = ucfirst(strtolower($value));
        });
        return implode('-', $words);
    }

    public function setMimeType($mime) {
        $this->mime = $mime;
        $this->setHeader('Content-Type', $mime . '; charset=UTF-8');
    }
    
    public function getHeaders() {
        return $this->headers;
    }

    public function getBody() {
        if($this->isBuffered())
            return $this->getBuffer();
        
        return $this->body;
    }

    public function getCode() {
        return $this->code;
    }

    public function getMimeType() {
        return $this->mime;
    }
    
    public function setError(Exception $error) {
        if($error instanceof HttpException)
            $this->setCode($error->getCode());
        else
            $this->setCode(Output::CODE_INTERNAL_ERROR);
        
        $this->setBody($error);
        
        if($this->propagateErrors)
            throw $error;
    }
    
    public function hasError() {
        return OutputUtils::isErrorCode($this->code);
    }

    public function getError() {
        return $this->body;
    }

    public function getHeader($header) {
        return $this->headers[$this->normalizeHeader($header)];
    }
}
