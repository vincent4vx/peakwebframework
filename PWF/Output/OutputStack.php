<?php

/*
 * The MIT License
 *
 * Copyright 2016 vincent.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace PWF\Output;

/**
 * The Output instances stack
 * This components should be use instead of directly use Output instance
 *
 * @author vincent
 */
class OutputStack implements Output{
    /**
     * @var \SplStack
     */
    private $stack;
    
    public function __construct(Output $appOutput) {
        $this->stack = new \SplStack();
        $this->stack->push($appOutput);
    }
    
    /**
     * Add an Output instance to the stack
     * @param Output $output
     */
    public function push(Output $output){
        $this->stack->push($output);
    }
    
    /**
     * Remove the top of the stack
     * @return Output
     * @throws \UnderflowException When try to pop the app output (the bottom of the stack)
     */
    public function pop(){
        if($this->stack->bottom() === $this->stack->top())
            throw new \UnderflowException('Cannot pop the app outout instance');
        
        return $this->stack->pop();
    }
    
    /**
     * Get the current Output instance (top of the stack)
     * @return Output
     */
    public function current(){
        return $this->stack->top();
    }
    
    //Delegates methods
    
    public function getMimeType() {
        return $this->current()->getMimeType();
    }

    public function hasError() {
        return $this->current()->hasError();
    }

    public function removeHeader($header) {
        $this->current()->removeHeader($header);
    }

    public function setBody($body) {
        $this->current()->setBody($body);
    }

    public function setCode($code) {
        $this->current()->setCode($code);
    }

    public function setError(\Exception $error) {
        $this->current()->setError($error);
    }

    public function setHeader($header, $value) {
        $this->current()->setHeader($header, $value);
    }

    public function setMimeType($mime) {
        $this->current()->setMimeType($mime);
    }
    
    public function getBody() {
        return $this->current()->getBody();
    }

    public function getBuffer() {
        return $this->current()->getBuffer();
    }

    public function getCode() {
        return $this->current()->getCode();
    }

    public function getError() {
        return $this->current()->getError();
    }

    public function getHeader($header) {
        return $this->current()->getHeader($header);
    }

    public function isBuffered() {
        return $this->current()->isBuffered();
    }

    public function resetBuffer() {
        $this->current()->resetBuffer();
    }

    public function setBuffered($b = true) {
        $this->current()->setBuffered($b);
    }

}
