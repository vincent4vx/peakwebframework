<?php

/*
 * The MIT License
 *
 * Copyright 2016 vincent.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace PWF\Output\OutputFormater;

use PWF\Output\OutputFormater\OutputFormater;
use PWF\Kernel;
use PWF\Output\Output;
use PWF\Loader;

/**
 * Description of MimeFormater
 *
 * @author vincent
 * 
 * @config output.format[]
 * @config output.format.default default PlainTextFormater::class
 */
class MimeFormater implements OutputFormater{
    private $config;

    /**
     * @var Loader
     */
    private $loader;

    public function __construct(Kernel $kernel, Loader $loader) {
        $this->loader = $loader;
        
        $this->config = $kernel->conf('output.format', [
            'default' => PlainTextFormater::class,
            Output::MIME_JSON => JsonFormater::class
        ]);
    }
    
    public function formatOutput(Output $output, $data) {
        $formater = isset($this->config[$output->getMimeType()]) ? $this->config[$output->getMimeType()] : $this->config['default'];
        
        return $this->loader->getInstance($formater)->formatOutput($output, $data);
    }
}
