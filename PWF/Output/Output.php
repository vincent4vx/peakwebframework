<?php

/*
 * The MIT License
 *
 * Copyright 2015 Vincent Quatrevieux <quatrevieux.vincent@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace PWF\Output;

/**
 * The Output interface
 * @author Vincent Quatrevieux <quatrevieux.vincent@gmail.com>
 */
interface Output {
    const CODE_OK                   = 200;
    const CODE_CREATED              = 201;
    const CODE_NO_CONTENT           = 204;
    
    const CODE_MOVE_PERMANENT       = 301;
    const CODE_MOVE_TEMP            = 302;
    
    const CODE_BAD_REQUEST          = 400;
    const CODE_UNAUTHORIZED         = 401;
    const CODE_FORBIDDEN            = 403;
    const CODE_NOT_FOUND_ERROR      = 404;
    const CODE_METHOD_NOT_ALLOWED   = 405;
    
    const CODE_INTERNAL_ERROR       = 500;
    
    const MIME_HTML = 'text/html';
    const MIME_TEXT = 'text/plain';
    const MIME_JSON = 'application/json';
    
    /**
     * Mime for internal requests (i.e. HMVC), should not be used with app output
     */
    const MIME_INTERNAL = '__Internal__';

    /**
     * Set an header value
     */
    public function setHeader($header, $value);
    
    /**
     * Remove an header
     */
    public function removeHeader($header);
    
    /**
     * Get the header value
     * @param string $header The header name
     */
    public function getHeader($header);
    
    /**
     * Set a return HTTP Code
     */
    public function setCode($code);
    
    /**
     * Get the HTTP Code
     */
    public function getCode();
    
    /**
     * Check the status code
     * @return bool true if the code is 2xx or 3xx
     */
    public function hasError();
    
    /**
     * Set a mime type
     */
    public function setMimeType($mime);
    
    /**
     * Get the mime type
     * @return string
     */
    public function getMimeType();
    
    /**
     * Set the body. Is has an old value (and if is it possible) replace the content
     * @param mixed $body The body value. The type should be supported by the Output instance
     */
    public function setBody($body);
    
    /**
     * Get the body value
     */
    public function getBody();
    
    /**
     * Set an error. 
     * The Output should set the correct HTTP code and the body
     * @warning The user shouldn't change the http code nor the body after setError
     * @param \Exception $error
     */
    public function setError(\Exception $error);
    
    /**
     * Get the error value.
     * This value should be the same as getBody() when error occurs, or null
     * @return \Exception|null The error as exception, or null is there is no errors
     * @see Output::hasError() To check is there is an error
     */
    public function getError();
    
    /**
     * Start or stop the buffer
     * @param bool $b The buffering state
     */
    public function setBuffered($b = true);
    
    /**
     * Check if the buffer is enabled
     * @return bool true is buffer is enabled, or false
     */
    public function isBuffered();
    
    /**
     * Get the buffer contents
     */
    public function getBuffer();
    
    /**
     * Reset the buffer contents
     */
    public function resetBuffer();
}
