<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace PWF\Output;

/**
 * Output class for CLI apps
 * @author vincent
 */
class ConsoleOutput implements Output{
    private $code = Output::CODE_OK;

    /**
     * @var \Exception|null
     */
    private $error = null;
    
    public function __construct() {
    }
    
    public function getMimeType() {}

    public function removeHeader($header) {}

    public function setBody($body) {
        echo $body;
    }

    public function setCode($code) {
        $this->code = $code;
    }

    public function setError(\Exception $error) {
        $this->error = $error;
    }

    public function setHeader($header, $value) {}

    public function setMimeType($mime) {}
    
    public function hasError() {
        $base = (int)($this->code / 100);
        return $base !== 2 && $base !== 3;
    }
    
    public function getBody() {
        
    }

    public function getBuffer() {
        
    }

    public function getCode() {
        
    }

    public function getError() {
       return $this->error; 
    }

    public function getHeader($header) {
        
    }

    public function isBuffered() {
        
    }

    public function resetBuffer() {
        
    }

    public function setBuffered($b = true) {
        
    }
}
