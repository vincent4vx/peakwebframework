<?php

/*
 * The MIT License
 *
 * Copyright 2015 Vincent Quatrevieux <quatrevieux.vincent@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace PWF\Output\Template;

use PWF\Constants;
use PWF\Executor\Middleware\Middleware;
use PWF\Input\HttpInput;
use PWF\Kernel;
use PWF\Output\Output;
use PWF\Widget\Widgets;

/**
 * Handle templates rendering
 *
 * @author Vincent Quatrevieux <quatrevieux.vincent@gmail.com>
 */
class Template implements Middleware{
    /**
     *
     * @var Kernel
     */
    private $kernel;

    private $layout = null;
    private $themes;
    private $baseDir;
    
    private $vars = [];
    
    public function __construct(Kernel $kernel) {
        $this->kernel = $kernel;
        
        $this->themes = $kernel->conf('template.themes', ['default']);
        $this->layout = $kernel->conf('template.default_layout');
        $this->baseDir = $kernel->conf('template.directory', Constants::ROOT_DIR . 'App' . DIRECTORY_SEPARATOR . 'Templates' . DIRECTORY_SEPARATOR);
        ob_start();
        
        $this->kernel->middlewares->after($this);
    }
    
    public function __get($name) {
        return $this->kernel->$name;
    }
    
    public function conf($key, $default = null){
        return $this->kernel->conf($key, $default);
    }
    
    public function setLayout($layout) {
        $this->layout = $layout;
    }

    public function setTheme($theme){
        array_unshift($this->themes, $theme);
    }
    
    public function render($__template, array $__tplVars = []){
        foreach($this->themes as $theme) {
            $__tplFile = $this->baseDir . $theme . DIRECTORY_SEPARATOR . $__template;
            
            if(is_file($__tplFile)){
                ob_start();
                extract($__tplVars);
                require $__tplFile;
                return ob_get_clean();
            }
        }
        
        throw new TemplateNotFound($__template);
    }
    
    public function widget($name, array $vars = []){
        return $this->kernel->loader->getInstance(Widgets::class)->show($name, $vars);
    }
    
    public function setLayoutVar($name, $value){
        $this->vars[$name] = $value;
    }
    
    public function concactVar($name, $value){
        if(isset($this->vars[$name]))
            $this->vars[$name] .= $value;
        else
            $this->vars[$name] = $value;
    }
    
    public function setTitle($title){
        $this->vars['title'] = $title;
    }
    
    public function flush(Output $output) {
        $contents = ob_get_clean();
        $this->vars['contents'] = $contents;
        
        if(empty($contents)){
            $output->setCode(Output::CODE_NO_CONTENT);
            return;
        }
        
        if($this->layout === null)
            $output->setBody($contents);
        else
            $output->setBody($this->render($this->layout, $this->vars));
    }
    
    public function priority() {
        return -10;
    }

    public function run(HttpInput $input = null, Output $output = null) {
        $this->flush($output);
    }
}
