<?php

/* 
 * The MIT License
 *
 * Copyright 2016 vincent.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace PWF\Output\Util;

/**
 * Trait to handle buffer on Output classes
 */
trait OutputBufferTrait{
    private $isBuffered = false;
    
    public function getBuffer() {
        if(!$this->isBuffered)
            throw new InvalidStateException(get_class() . ' : The buffer is not activated');
        
        return ob_get_contents();
    }
    
    public function isBuffered() {
        return $this->isBuffered;
    }

    public function resetBuffer() {
        if(!$this->isBuffered)
            throw new InvalidStateException(get_class() . ' : The buffer is not activated');
        
        ob_clean();
    }

    public function setBuffered($b = true) {
        if($this->isBuffered == $b)
            return;
        
        if($b){
            ob_start();
            $this->setBody(null);
            $this->isBuffered = true;
        }else{
            $contents = ob_get_clean();
            $this->setBody($contents);
            $this->isBuffered = false;
        }
    }
}