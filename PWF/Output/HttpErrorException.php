<?php

/*
 * The MIT License
 *
 * Copyright 2015 Vincent Quatrevieux <quatrevieux.vincent@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace PWF\Output;

/**
 * Description of HttpErrorException
 *
 * @author Vincent Quatrevieux <quatrevieux.vincent@gmail.com>
 */
class HttpErrorException extends HttpException{
    private $errorFile, $errorLine, $errorType, $stackTrace, $errorContext;

    public function __construct($message, $type, $file, $line, array $stackTrace = [], array $context = []) {
        parent::__construct($message, 500);
        $this->errorType = $type;
        $this->errorFile = $file;
        $this->errorLine = $line;
        $this->stackTrace = $stackTrace;
        $this->errorContext = $context;
    }
    
    public function jsonSerialize() {
        return array_merge(parent::jsonSerialize(), [
            'type' => $this->errorType,
            'file' => $this->errorFile,
            'line' => $this->errorLine
        ]);
    }
    
    public function getErrorFile() {
        return $this->errorFile;
    }

    public function getErrorLine() {
        return $this->errorLine;
    }

    public function getErrorType() {
        return $this->errorType;
    }
    
    public function getErrorStackTrace(){
        return $this->stackTrace;
    }
    
    public function getErrorContext() {
        return $this->errorContext;
    }
}
