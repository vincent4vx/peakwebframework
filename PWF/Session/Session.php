<?php

/*
 * The MIT License
 *
 * Copyright 2015 Vincent Quatrevieux <quatrevieux.vincent@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace PWF\Session;

/**
 * Session interface
 * @author Vincent Quatrevieux <quatrevieux.vincent@gmail.com>
 */
interface Session {
    /**
     * Get a session var value
     * @param string $name The session var name
     * @return mixed|null Return the session value, or null if not exists
     */
    public function __get($name);
    
    /**
     * Check if a session value exists
     * @param string $name The session var name
     * @return boolean true if exists, or false
     */
    public function __isset($name);
    
    /**
     * Set a new value into a session var
     * @param string $name The session var name
     * @param mixed $value The new value
     */
    public function __set($name, $value);
    
    /**
     * Remove a session var
     * @param string $name The session var name
     */
    public function __unset($name);

    /**
     * Set multiple values into session
     * @param array $values Values in format [name] => [value]
     */
    public function setAll(array $values);
    
    /**
     * Clear all session data and keep the session
     */
    public function clear();
    
    /**
     * Clear all session data AND destroy the session
     */
    public function destroy();
    
    /**
     * Get the session id
     */
    public function sessId();
}
