<?php

/*
 * The MIT License
 *
 * Copyright 2015 Vincent Quatrevieux <quatrevieux.vincent@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace PWF\Session;

use PWF\Kernel;

/**
 * Session implemtation using PHP functions
 *
 * @author Vincent Quatrevieux <quatrevieux.vincent@gmail.com>
 */
class PhpSession implements Session{
    const IP_INDEX = '__ip';
    const LAST_UPDATE_INDEX = '__lastUpdate';
    const DATA_INDEX = '__data';
    
    private $config;
    
    public function __construct(Kernel $kernel) {
        $this->config = $kernel->conf('session', [
            'duration' => 7200,
            'check_ip' => true
        ]);
        
        session_start();
        
        if(!isset($_SESSION[self::IP_INDEX], $_SESSION[self::LAST_UPDATE_INDEX], $_SESSION[self::DATA_INDEX]))
            $this->create();
        elseif(!$this->check())
            $this->destroy();
        else
            $_SESSION[self::LAST_UPDATE_INDEX] = time();
    }
    
    private function create(){
        $_SESSION[self::IP_INDEX] = $_SERVER['REMOTE_ADDR'];
        $_SESSION[self::LAST_UPDATE_INDEX] = time();
        $_SESSION[self::DATA_INDEX] = [];
    }
    
    private function check(){
        if($this->config['duration'] > 0 
            && time() - $_SESSION[self::LAST_UPDATE_INDEX] > $this->config['duration'])
            return false;
        
        if($this->config['check_ip'] 
            && $_SERVER['REMOTE_ADDR'] !== $_SESSION[self::IP_INDEX])
            return false;
        
        return true;
    }

    public function __get($name) {
        return isset($_SESSION[self::DATA_INDEX][$name]) ? $_SESSION[self::DATA_INDEX][$name] : null;
    }

    public function __isset($name) {
        return isset($_SESSION[self::DATA_INDEX][$name]);
    }

    public function __set($name, $value) {
        $_SESSION[self::DATA_INDEX][$name] = $value;
    }

    public function __unset($name) {
        unset($_SESSION[self::DATA_INDEX][$name]);
    }

    public function clear() {
        $_SESSION[self::DATA_INDEX] = [];
    }

    public function destroy() {
        $this->clear();
        session_destroy();
        session_start();
        $this->create();
    }

    public function sessId() {
        return session_id();
    }

    public function setAll(array $values) {
        array_replace($_SESSION[self::DATA_INDEX], $values);
    }
}
