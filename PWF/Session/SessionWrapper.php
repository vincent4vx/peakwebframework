<?php

/*
 * The MIT License
 *
 * Copyright 2015 Vincent Quatrevieux <quatrevieux.vincent@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace PWF\Session;

/**
 * Delegate all session operation to the Session implementation
 * to help extending Session with helpers functions (like load user)
 *
 * @author Vincent Quatrevieux <quatrevieux.vincent@gmail.com>
 */
abstract class SessionWrapper implements Session{
    /**
     *
     * @var Session
     */
    private $session;
    
    public function __construct(Session $session) {
        $this->session = $session;
    }
    
    public function __get($name) {
        return $this->session->$name;
    }

    public function __isset($name) {
        return isset($this->session->$name);
    }

    public function __set($name, $value) {
        $this->session->$name = $value;
    }

    public function __unset($name) {
        unset($this->session->$name);
    }

    public function clear() {
        $this->session->clear();
    }

    public function destroy() {
        $this->session->destroy();
    }

    public function sessId() {
        return $this->session->sessId();
    }

    public function setAll(array $values) {
        $this->session->setAll($values);
    }
}
