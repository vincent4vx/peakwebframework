<?php

/*
 * The MIT License
 *
 * Copyright 2015 Vincent Quatrevieux <quatrevieux.vincent@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace PWF;

use InvalidArgumentException;
use PWF\Debug\ErrorHandler;
use PWF\Exception\InvalidOperationException;
use PWF\Executor\Executor;
use PWF\Executor\ExecutorImpl;
use PWF\Executor\Middleware\Middlewares;
use PWF\Executor\Middleware\MiddlewaresStack;
use PWF\Input\HttpInput;
use PWF\Input\InputStack;
use PWF\Output\HttpNotFound;
use PWF\Output\InternalOutput;
use PWF\Output\Output;
use PWF\Output\OutputStack;
use PWF\Router\Route;
use PWF\Router\RouteImpl;

/**
 * Base class for PWF
 *
 * @author Vincent Quatrevieux <quatrevieux.vincent@gmail.com>
 * 
 * @property-read Loader $loader The Loader
 * @property-read array $config Configuration
 * @property-read ErrorHandler $errorHandler
 * @property-read Output $app_output
 * @property-read HttpInput $app_input
 * @property-read Route $app_route
 * @property-read Executor $executor
 * @property-read Middlewares $middlewares
 */
abstract class AbstractKernel implements Kernel {
    protected $reg = [];
    
    public function __construct(Loader $loader, array $config) {
        $this->loader = $loader;
        $this->config = $config;
        
        $this->loader->setInstance($this);
        $this->loader->setModule(Kernel::class, get_class($this));
        
        $this->loader->setModules($this->conf('modules', $this->getDefaultModules()));
        
        $this->app_input = $loader->getInstance(HttpInput::class);
        $this->app_output = $loader->getInstance(Output::class);
        $this->errorHandler = $loader->getInstance(ErrorHandler::class);
        
        $this->executor = $loader->getInstance(Executor::class);
        $this->middlewares = $loader->getInstance(Middlewares::class);
        
        foreach($this->conf('preload', []) as $name => $class){
            $this->$name = $this->loader->getInstance($class);
        }
        
        $this->registerMiddlwares();
        
        $this->middlewares->run(Middlewares::EVENT_STARTUP);
    }
    
    /**
     * Register the default modules
     * @return array
     */
    protected function getDefaultModules(){
        return [
            Executor::class => ExecutorImpl::class,
            Middlewares::class => MiddlewaresStack::class
        ];
    }
    
    /**
     * Register the defaults middlewares
     */
    protected function registerMiddlwares(){
        $this->middlewares->buildFromConfig($this->loader, $this->conf('middlewares', []));
    }
    
    public function __get($name) {
        if(!isset($this->reg[$name]))
            return null;
        
        return $this->reg[$name];
    }
    
    public function __set($name, $value) {
        if(isset($this->reg[$name]))
            throw new InvalidOperationException($name . ' has already a value into the registry. Use AbstractKernel->forceSet() instead.');
        
        $this->reg[$name] = $value;
    }
    
    public function __isset($name) {
        return isset($this->reg[$name]);
    }
    
    public function forceSet($name, $value){
        $this->reg[$name] = $value;
    }
    
    public function conf($key, $default = null){
        $item = $this->config;
        
        foreach(explode('.', $key) as $k){
            if(!is_array($item) || !isset($item[$k]))
                return $default;
            
            $item = $item[$k];
        }
        
        if(!is_array($default))
            return $item;
        
        return array_replace_recursive($default, $item);
    }
    
    /**
     * Run a web page, using the controller name, the action method, the output instance and the args
     * 
     * @config app.controller_ns defaults 'App\Controller'
     * @config app.controller_suffix defaults ''
     * @config app.action_suffix defaults 'Action'
     * 
     * @param type $controller
     * @param string $action
     * @param array $args
     * @param Output $output
     * @return Output
     * @throws HttpNotFound
     * @throws InvalidArgumentException
     * @deprecated
     */
    protected function baseRun($controller, $action, array $args, Output $output = null, HttpInput $input = null){
        trigger_error('Call deprecated method ' . __METHOD__ . '. Use Executor instead', E_USER_DEPRECATED);
        
        if($output !== null)
            $this->loader->getInstance(OutputStack::class)->push($output);

        if($input !== null)
            $this->loader->getInstance(InputStack::class)->push($input);
        
        try{
            $ctrlClass = $this->conf('app.controller_ns', 'App\Controller') . '\\' . ucfirst(strtolower($controller)) . $this->conf('app.controller_suffix', '');

            $action = strtolower($action) . $this->conf('app.action_suffix', 'Action');

            $response = $this->executor->route(new RouteImpl($ctrlClass, $action, $args));

            if($response !== null)
                $this->loader->getInstance(OutputStack::class)->current()->setBody($response);

            return $this->loader->getInstance(OutputStack::class)->current();
        }finally{
            if($output !== null)
            $this->loader->getInstance(OutputStack::class)->pop();

        if($input !== null)
            $this->loader->getInstance(InputStack::class)->pop();
        }
    }
    
    public function run($controller, $action, array $args = []) {
        return $this->baseRun($controller, $action, $args, $this->loader->newInstance(InternalOutput::class));
    }
    
    /**
     * Run the web page using a Router
     * 
     * @deprecated Use execute() without parameters 
     */
    public function runRoute(){
        return $this->execute();
    }
    
    public function execute(HttpInput $input = null, Output $output = null) {
        return $this->executor->app($input, $output);
    }
    
    public function __destruct() {
        $this->middlewares->run(Middlewares::EVENT_SHUTDOWN);
    }
}
