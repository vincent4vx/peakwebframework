<?php

/*
 * The MIT License
 *
 * Copyright 2015 Vincent Quatrevieux <quatrevieux.vincent@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace PWF;

use PWF\Input\HttpInput;
use PWF\Router\Router;
use PWF\Output\HttpOutput;
use PWF\Router\HttpPathRouter;
use PWF\Input\BasicHttpInput;
use PWF\Output\Output;
use PWF\Debug\ErrorFormater\ErrorFormater;
use PWF\Debug\ErrorFormater\ErrorFormaterComposite;
use PWF\Output\OutputFormater\OutputFormater;
use PWF\Output\OutputFormater\MimeFormater;

/**
 * Kernel for web apps
 *
 * @author Vincent Quatrevieux <quatrevieux.vincent@gmail.com>
 * 
 * @property-read HttpInput $input
 * @property-read Router $router
 */
class WebKernel extends AbstractKernel{
    public function __construct(Loader $loader, array $config) {
        parent::__construct($loader, $config);
        $this->input = $this->loader->getInstance(HttpInput::class);
        $this->router = $this->loader->getInstance(Router::class);
    }
    
    protected function getDefaultModules() {
        return array_merge(parent::getDefaultModules(), [
            HttpInput::class => BasicHttpInput::class,
            Router::class => HttpPathRouter::class,
            ErrorFormater::class => ErrorFormaterComposite::class,
            OutputFormater::class => MimeFormater::class,
            Session\Session::class => Session\PhpSession::class,
            Cache\Cache::class => Cache\FileCache::class,
            Output::class => HttpOutput::class
        ]);
    }
}
