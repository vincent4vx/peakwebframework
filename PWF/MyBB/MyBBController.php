<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace PWF\MyBB;

use PWF\MyBBKernel;
use PWF\MyBB\MyBBTemplates;
use PWF\Arch\Controller;

/**
 * Base controller for apps using MyBB Kernel
 *
 * @author vincent
 */
abstract class MyBBController extends Controller{
    /**
     *
     * @var MyBBTemplates
     */
    protected $templates;
    
    /**
     *
     * @var \PWF\MyBBKernel
     */
    protected $kernel;


    public function __construct(MyBBKernel $kernel, MyBBTemplates $templates) {
        parent::__construct($kernel);
        $this->kernel = $kernel;
        $this->templates = $templates;
    }
    
    /**
     * 
     * @param type $template
     * @param array $vars
     * @return type
     * @see MyBBTemplates::render()
     */
    public function render($template, array $vars = []){
        return $this->templates->render($template, $vars);
    }
    
    /**
     * @param string $structure structure filename
     * @param array $vars
     * @return string
     * @see MyBBTemplates::structure()
     */
    public function structure($structure, array $vars = []){
        return $this->templates->structure($structure, $vars);
    }
}
