<?php global $headerinclude, $mybb, $header, $footer?>
<html>
    <head>
        <title><?= htmlentities($mybb->settings['bbname'])?><?php if(!empty($title)) echo ' - ', htmlentities($title)?></title>
        <base href="<?= htmlentities($mybb->settings['bburl'])?>"/>
        <?= $headerinclude?>
        <?php if(!empty($header_scripts)) echo $header_scripts?>
    </head>
    <body>
        <?= $header?>
        <?= $contents?>
        <?= $footer?>
        <?php if(!empty($footer_scripts)) echo $footer_scripts?>
    </body>
</html>