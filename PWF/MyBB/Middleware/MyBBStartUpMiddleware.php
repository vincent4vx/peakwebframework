<?php

/*
 * The MIT License
 *
 * Copyright 2016 vincent.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace PWF\MyBB\Middleware;

use PWF\Exception\InvalidOperationException;
use PWF\Executor\Middleware\Middleware;
use PWF\Input\HttpInput;
use PWF\Kernel;
use PWF\MyBB\Plugin\MyBBHooks;
use PWF\Output\Output;

/**
 * Middleware for startup MyBBKernel
 *
 * @author vincent
 */
class MyBBStartUpMiddleware implements Middleware{
    /**
     * @var Kernel
     */
    private $kernel;
    
    public function __construct(Kernel $kernel) {
        $this->kernel = $kernel;
    }

    public function __set($name, $value) {
        $this->kernel->$name = $value;
    }
    
    public function __get($name) {
        return $this->kernel->$name;
    }
    
    public function __call($name, $arguments) {
        return call_user_func_array([$this->kernel, $name], $arguments);
    }
    
    public function priority() {
        return 10;
    }

    public function run(HttpInput $input = null, Output $output = null) {
        //set global vars
        global $mybb, $error_handler, $maintimer, $config, $templates, $db, 
                $plugins, $cache, $lang, $settings, $version, $date_formats, $time_formats;
        
        if(!defined('IN_MYBB') || !isset($mybb))
            throw new InvalidOperationException('You should require MyBB global.php script before');
        
        $this->loader->setInstance($mybb);
        $this->mybb = $mybb;
        
        $this->loader->setInstance($error_handler);
        $this->mybb_error_handler = $error_handler;
        
        $this->loader->setInstance($maintimer);
        $this->mybb_maintimer = $maintimer;
        
        $this->mybb_config = $config;
        
        $this->loader->setInstance($templates);
        $this->mybb_templates = $templates;
        
        $this->mybb_db = $db;
        
        $this->loader->setInstance($plugins);
        $this->mybb_plugins = $plugins;
        
        $this->loader->setInstance($cache);
        $this->mybb_cache = $cache;
        
        $this->loader->setInstance($lang);
        $this->mybb_lang = $lang;
        
        $this->mybb_settings = $settings;
        $this->mybb_version = $version;
        $this->mybb_date_formats = $date_formats;
        $this->mybb_time_formats = $time_formats;
        
        //Components when global is loaded
        $this->loader->getInstance(MyBBHooks::class)->add('global_start', [$this, 'globalStartHook']);
    }
    
    public function globalStartHook(){
        global $session;
        
        $this->loader->setInstance($session);
        $this->mybb_session = $session;
        
        foreach($this->conf('mybb.langs') as $file){
            $this->mybb_lang->load($file, false, true);
        }
    }
}
