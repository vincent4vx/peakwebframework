<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace PWF\MyBB;

use MyLanguage;
use PWF\Constants;
use PWF\Executor\Middleware\Middleware;
use PWF\Executor\Middleware\Middlewares;
use PWF\Input\HttpInput;
use PWF\Kernel;
use PWF\Output\Output;
use PWF\Output\Template\TemplateNotFound;
use PWF\Widget\Widgets;
use templates;

/**
 * Adapt the MyBB template system to PWF
 * @author vincent
 */
class MyBBTemplates{
    /**
     * MyBB templates sytem
     * @var templates
     */
    private $mybb_templates;
    
    /**
     *
     * @var Kernel
     */
    private $kernel;
    
    /**
     * @var MyLanguage
     */
    private $lang;
    
    public function __construct(templates $mybb_templates, Kernel $kernel, MyLanguage $lang, Middlewares $middlewares) {
        $this->mybb_templates = $mybb_templates;
        $this->kernel = $kernel;
        $this->lang = $lang;
    }
    
    public function __get($name) {
        return $this->kernel->$name;
    }
    
    public function __call($name, $arguments) {
        return call_user_func_array([$this->kernel, $name], $arguments);
    }
    
    private function addToTplList($tpl){
        global $templatelist;
        
        if(empty($templatelist))
            $templatelist = $tpl;
        else
            $templatelist .= ',' . $tpl;
    }
    
    public function tr($key){
        $str = $this->kernel->mybb_lang->$key;
        
        if(func_num_args() > 1){
            return $this->kernel->mybb_lang->sprintf($str, array_slice(func_get_args(), 1));
        }
        
        return $str;
    }
    
    public function structure($structure, array $vars = []){
        $file = $this->kernel->conf('mybb.structure_dir') . $structure;
        
        if(!is_file($file))
            throw new TemplateNotFound($file);
        
        extract($vars);
        
        ob_start();
        require $file;
        return ob_get_clean();
    }
    
    public function render($template, array $vars = []){
        foreach(array_keys($this->mybb_templates->cache) as $t)
            global $$t;

        //auto install template during dev
        if(Constants::DEBUG && !isset($this->mybb_templates->cache[$template])){
            if($this->isLocalTemplate($template))
                $this->installTemplate ($template);
        }
        
        $this->addToTplList($template);
        
        global $mybb, $lang, $theme;
        extract($vars);
        
        $rendered = eval('return "' . $this->mybb_templates->get($template) . '";');
        
        return $rendered;
    }
    
    public function installTemplate($name){
        if(!$this->isLocalTemplate($name))
            throw new TemplateNotFound($name);
        
        $this->kernel->mybb_db->delete_query('templates', 'title=\'' . $this->kernel->mybb_db->escape_string($name) . '\'');
        $this->kernel->mybb_db->insert_query('templates', [
            'title' => $this->kernel->mybb_db->escape_string($name),
            'sid' => '-1',
            'template' => $this->kernel->mybb_db->escape_string(file_get_contents($this->kernel->conf('mybb.templates_dir') . $name . '.html')),
            'version' => $this->kernel->mybb_version['version_code'],
            'dateline' => TIME_NOW
        ]);
    }
    
    private function isLocalTemplate($name){
        return is_file($this->kernel->conf('mybb.templates_dir') . $name . '.html');
    }
    
    public function addBreadcrumb($title, $url = ''){
        add_breadcrumb($title, $url);
    }
    
    public function widget($name, array $vars = []){
        return $this->kernel->loader->getInstance(Widgets::class)->show($name, $vars);
    }
}
