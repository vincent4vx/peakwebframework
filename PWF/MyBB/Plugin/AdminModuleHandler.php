<?php

/*
 * The MIT License
 *
 * Copyright 2016 vincent.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace PWF\MyBB\Plugin;

use PWF\MyBB\MyBBAdminRegistry;
use PWF\Output\HttpNotFound;
use PWF\Input\HttpInput;

/**
 * Description of AdminModuleHook
 *
 * @author vincent
 */
class AdminModuleHandler {
    /**
     *  @var AdminModule[]
     */
    static private $modules = [];

    /**
     *
     * @var MyBBHooks
     */
    private $hooks;
    
    /**
     *
     * @var \PWF\MyBB\MyBBAdminRegistry
     */
    private $reg;
    
    /**
     * @var HttpInput
     */
    private $input;
    
    public function __construct(MyBBHooks $hooks, MyBBAdminRegistry $reg, HttpInput $input) {
        $this->hooks = $hooks;
        $this->reg = $reg;
        $this->input = $input;
        $this->registerHooks();
    }
    
    private function registerHooks(){
        if(!defined('IN_ADMINCP'))
            return;
        
        $this->hooks->add('admin_tabs', [$this, 'adminTabsHook']);
        $this->hooks->add('admin_load', [$this, 'adminLoadHook']);
    }

    public function registerModule(AdminModule $module){
        if(!defined('IN_ADMINCP'))
            return;
        
        self::$modules[$module->name()] = $module;
        
        $class = __CLASS__;
        
        $code = <<<CODE
function {$module->name()}_action_handler(\$action){
    \$module = {$class}::getModule('{$module->name()}');
    \$module->actionHandler(\$action);
}
CODE;
    
        eval($code);
    }
    
    public function adminTabsHook(array &$modules){
        if(!defined('IN_ADMINCP'))
            return;
        
        foreach(self::$modules as $module){
            $modules[$module->name()] = 1;
            $this->reg->page->add_menu_item(
                $module->l10nName(), 
                $module->name(), 
                'index.php?module=' . $module->name(),
                $module->priority(),
                $module->menu()
            );
        }
        
        return $modules;
    }
    
    public function adminLoadHook(){
        if(!defined('IN_ADMINCP'))
            return;
        
        if(!isset(self::$modules[$this->reg->page->active_module]))
            return;
        
        $module = self::$modules[$this->reg->page->active_module];
        $action = $module->getAction($this->reg->page->active_action);
        
        if($action === null)
            return;
        
        $this->reg->page->add_breadcrumb_item($module->l10nName(), 'index.php?module=' . $module->name());
        $this->reg->page->add_breadcrumb_item($action->title(), 'index.php?module=' . $module->name() . '-' . $action->id());
        
        $method = 'defaultAction';
        
        if($this->input->get('action'))
            $method = $this->input->get('action') . 'Action';
        
        if(!method_exists($action, $method))
            $method = 'defaultAction';
        
        $action->$method();
    }
    
    static public function getModule($name){
        return self::$modules[$name];
    }
}
