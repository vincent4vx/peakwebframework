<?php

/*
 * The MIT License
 *
 * Copyright 2016 vincent.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace PWF\MyBB\Plugin;

use PWF\MyBB\MyBBAdminRegistry;
use PWF\Loader;
use PWF\Constants;

/**
 * Description of AbstractAdminModule
 *
 * @author vincent
 */
abstract class AbstractAdminModule implements AdminModule{
    /**
     * @var MyBBAdminRegistry
     */
    protected $reg;
    
    /**
     *
     * @var Loader
     */
    protected $loader;
    
    /**
     *  @var AdminModuleAction[]
     */
    protected $actions;

    public function __construct(MyBBAdminRegistry $reg, Loader $loader, array $actionClasses) {
        $this->reg = $reg;
        $this->loader = $loader;
        $this->actions = $this->loadActions($actionClasses);
    }
    
    abstract public function defaultActionName();
    
    private function loadActions(array $classes){
        $actions = [];
        
        foreach($classes as $class){
            $act = $this->loader->getInstance($class);
            
            if(Constants::DEBUG && !($act instanceof AdminModuleAction))
                throw new \InvalidArgumentException($class . ' should be a subclass of AdminModuleAction');
            
            $actions[$act->id()] = $act;
        }
        
        return $actions;
    }

    public function actionHandler($action) {
        $this->reg->page->active_module = $this->name();
        
        if(empty($this->actions[$action]))
            $action = $this->defaultActionName();
        
        $this->reg->page->active_action = $action;
        
        return null;
    }

    public function l10nName() {
        return $this->name();
    }
    
    public function menu() {
        $menu = [];
        
        foreach($this->actions as $action){
            $menu[] = [
                'link' => 'index.php?module=' . $this->name() . '-' . $action->id(),
                'title' => $action->title(),
                'id' => $action->id()
            ];
        }
        
        return $menu;
    }
    
    public function getAction($action) {
        return isset($this->actions[$action]) ? $this->actions[$action] : null;
    }
    
    public function priority() {
        return 100;
    }
}
