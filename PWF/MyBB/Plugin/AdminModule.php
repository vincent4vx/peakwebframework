<?php

/*
 * The MIT License
 *
 * Copyright 2016 vincent.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace PWF\MyBB\Plugin;

/**
 * Module for MyBB admin cp
 * @author vincent
 */
interface AdminModule {
    /**
     * Get the module name
     * Should be unique
     */
    public function name();
    
    /**
     * Get the localized name
     */
    public function l10nName();
    
    /**
     * The module's action handler
     * Should set $page->active_module and $page->active_action
     * @param string $action The action name
     * @return null
     */
    public function actionHandler($action);
    
    /**
     * Get the module menu
     * @return array
     * <code>
     *  $menu = [];
     *  $menu[] = [
     *      'link' => 'index.php?module=foo-bar',
     *      'id' => 'bar',
     *      'title' => 'Bar'
     *  ];
     * </code>
     */
    public function menu();
    
    /**
     * Get the current action
     * @param string $action The action name
     * @return AdminModuleAction
     */
    public function getAction($action);
    
    /**
     * The module priority (into tabs)
     * @return int
     */
    public function priority();
}
