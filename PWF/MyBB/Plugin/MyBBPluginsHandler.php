<?php

/*
 * The MIT License
 *
 * Copyright 2016 vincent.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace PWF\MyBB\Plugin;

use InvalidArgumentException;
use PWF\Loader;
use PWF\MyBBKernel;

/**
 * Handle MyBB plugins
 * @author vincent
 */
class MyBBPluginsHandler {
    /**
     * The plugins instances
     * @var Plugin[]
     */
    static private $plugins = [];
    
    /**
     * @var Loader
     */
    private $loader;
    
    /**
     * @var MyBBKernel
     */
    private $kernel;
    
    public function __construct(Loader $loader, MyBBKernel $kernel) {
        $this->loader = $loader;
        $this->kernel = $kernel;
    }
    
    /**
     * Get the plugin install
     * @param string $name The plugin name
     * @return Plugin
     */
    static public function getPlugin($name){
        if(!isset(self::$plugins[$name]))
            throw new InvalidArgumentException('The plugin ' . $name . ' is not registered');
        
        return self::$plugins[$name];
    }
    
    /**
     * Register and start the plugin
     * @param string $plugin Plugin class name
     */
    public function handle($plugin){
        
        $instance = $this->loader->getInstance($plugin);
        
        if(!($instance instanceof Plugin)){
            throw new InvalidArgumentException($plugin . ' is not a valid instance of Plugin');
        }
        
        if(isset(self::$plugins[$instance->name()])
                || function_exists($instance->name() . '_info')){
            throw new InvalidArgumentException('The plugin ' . $instance->name() . ' (' . $plugin .  ') is already handled');
        }
        
        self::$plugins[$instance->name()] = $instance;
        $handlerClass = __CLASS__;
        
        eval(<<<CODE
                function {$instance->name()}_info(){
                    return {$handlerClass}::getPlugin('{$instance->name()}')->info();
                }
                    
                function {$instance->name()}_activate(){
                    {$handlerClass}::getPlugin('{$instance->name()}')->activate();
                }
                    
                function {$instance->name()}_deactivate(){
                    {$handlerClass}::getPlugin('{$instance->name()}')->deactivate();
                }
CODE
);
        
        if($instance instanceof InstallablePlugin){
            eval(<<<CODE
                    function {$instance->name()}_install(){
                        {$handlerClass}::getPlugin('{$instance->name()}')->install();
                    }

                    function {$instance->name()}_is_installed(){
                        return {$handlerClass}::getPlugin('{$instance->name()}')->isInstalled();
                    }

                    function {$instance->name()}_uninstall(){
                        {$handlerClass}::getPlugin('{$instance->name()}')->uninstall();
                    }
CODE
);     
        }
        
        $instance->start();
    }
}
