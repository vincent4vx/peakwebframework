<?php

/*
 * The MIT License
 *
 * Copyright 2016 vincent.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace PWF\MyBB\Plugin;

use PWF\MyBBKernel;
use PWF\MyBB\MyBBAdminRegistry;

/**
 *
 * @author vincent
 * 
 * @property-read \PWF\Input\HttpInput $input
 * @property-read \PWF\Router\Router $router
 * @property-read \PWF\Output\Output $app_output
 * @property-read \PWF\Loader $loader
 * @property \MyBB $mybb MyBB instance
 * @property \errorHandler $mybb_error_handler MyBB error handler
 * @property \timer $mybb_maintimer MyBB maintimer
 * @property array $mybb_config MyBB config array
 * @property \templates $mybb_templates MyBB templates system
 * @property \DB_MySQL|\DB_MySQLi|\DB_PgSQL|\DB_SQLite $mybb_db MyBB database system
 * @property \pluginSystem $mybb_plugins MyBB plugins system
 * @property \datacache $mybb_cache MyBB cache system
 * @property \MyLanguage $mybb_lang MyBB i18n system
 * @property array $mybb_settings MyBB settings array
 * @property string $mybb_version MyBB version string
 * @property array $mybb_date_formats
 * @property array $mybb_time_formats
 * @property \session $mybb_session MyBB session system
 */
abstract class AbstractAdminModuleAction implements AdminModuleAction{
    /**
     * @var MyBBKernel
     */
    protected $kernel;
    
    /**
     * @var MyBBAdminRegistry
     */
    protected $reg;
    
    public function __construct(MyBBKernel $kernel, MyBBAdminRegistry $reg) {
        $this->kernel = $kernel;
        $this->reg = $reg;
    }
    
    public function __get($name) {
        return $this->kernel->$name;
    }
    
    public function __set($name, $value) {
        return $this->kernel->$name = $value;
    }
    
    public function conf($key, $default = null){
        return $this->kernel->conf($key, $default);
    }
    
    public function outputHeader($title = ''){
        $this->reg->page->output_header($title);
    }
    
    public function outputFooter($quit = true){
        $this->reg->page->output_footer($quit);
    }
    
    public function outputNavTabs(array $tabs = null, $active = null){
        if(!$active)
            $active = $this->input->get('action') ?: 'default';
        
        if($tabs === null)
            $tabs = $this->getTabsList();
        
        foreach($tabs as $key => $value){
            if(is_string($value)){
                $value = [
                    'title' => $value
                ];
            }
            
            if(!isset($value['link'])){
                $value['link'] = $this->helpers->url->url('', [
                    'module' => $this->reg->page->active_module . '-' . $this->id(),
                    'action' => $key
                ]);
            }
            
            $tabs[$key] = $value;
        }
        
        $this->reg->page->output_nav_tabs($tabs, $active);
    }
    
    protected function getTabsList(){
        return [];
    }
    
    public function addBreadcrumb($title, $url = ''){
        $this->reg->page->add_breadcrumb_item($title, $url = '');
    }
}
