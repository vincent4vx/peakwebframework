<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace PWF\MyBB\Plugin\Setting;

/**
 * Description of MyBBSettingsBuilder
 *
 * @author vincent
 */
class MyBBSettingsBuilder {
    /**
     * @var MyBBSettingsHandler
     */
    private $handler;
    
    /**
     * @var MyBBSettingsGroup
     */
    private $group;
    
    private $order = 1;
    
    public function __construct(MyBBSettingsHandler $handler, MyBBSettingsGroup $group) {
        $this->handler = $handler;
        $this->group = $group;
    }

    /**
     * @return MyBBSettingsBuilder
     */
    public function addSetting($name, $title, $description, $optionscode, $value){
        $setting = new MyBBSetting($this->group, $name, $title, $description, $optionscode, $value, $this->order);
        $this->handler->insertSetting($setting);
        ++$this->order;
        return $this;
    }
    
    /**
     * @return MyBBSettingsHandler
     */
    public function end(){
        rebuild_settings();
        return $this->handler;
    }
}
