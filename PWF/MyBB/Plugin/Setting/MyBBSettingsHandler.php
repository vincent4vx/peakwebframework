<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace PWF\MyBB\Plugin\Setting;

use MyBB;
use PWF\Database\MyBB\MyBBDatabase;

/**
 * Handler for MyBB settings
 * @author vincent
 */
class MyBBSettingsHandler {
    /**
     * @var MyBB
     */
    private $mybb;
    
    /**
     * @var MyBBDatabase
     */
    private $db;
    
    public function __construct(MyBB $mybb, MyBBDatabase $db) {
        $this->mybb = $mybb;
        $this->db = $db;
    }

    /**
     * Insert a Setting group into the BD and return the group
     * @param MyBBSettingsGroup $group
     * @return MyBBSettingsGroup
     */
    public function insertGroup(MyBBSettingsGroup $group){
        $id = $this->db->insertQuery('settinggroups', [
            'name' => $group->name(),
            'title' => $group->title(),
            'description' => $group->description(),
            'disporder' => $group->order(),
            'isdefault' => 0
        ]);
        
        return $group->fork($id);
    }
    
    /**
     * Insert the setting into DB
     * @param MyBBSetting $setting
     */
    public function insertSetting(MyBBSetting $setting){
        $this->db->insertQuery('settings', [
            'name' => $setting->name(),
            'title' => $setting->title(),
            'optionscode' => $setting->optionsCode(),
            'value' => $setting->value(),
            'description' => $setting->description(),
            'disporder' => $setting->disporder(),
            'gid' => $setting->group()->id()
        ]);
    }
    
    /**
     * Create a new setting group
     * @param string $name
     * @param string $title
     * @param string $description
     * @param int $order
     * @return MyBBSettingsBuilder
     */
    public function createGroup($name, $title, $description, $order){
        return new MyBBSettingsBuilder(
            $this, 
            $this->insertGroup(new MyBBSettingsGroup(-1, $name, $title, $description, $order))
        );
    }
    
    public function __get($name) {
        return $this->mybb->settings[$name];
    }
}
