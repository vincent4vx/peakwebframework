<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace PWF\MyBB\Plugin\Setting;

/**
 * Description of MyBBSetting
 *
 * @author vincent
 */
class MyBBSetting {
    const OPT_TEXT = 'text';
    const OPT_NUMERIC = 'numeric';
    const OPT_TEXTAREA = 'textarea';
    const OPT_YESNO = 'yesno';
    const OPT_SELECT = 'select';
    const OPT_FORUM_MULT = 'forumselect';
    const OPT_FORUM_SINGLE = 'forumselectsingle';
    const OPT_GROUPS = 'groupselect';
    const OPT_GROUP_SINGLE = 'groupselectsingle';
    const OPT_RADIO = 'radio';
    const OPT_CHECKBOX = 'checkbox';
    const OPT_LANG = 'language';
    const OPT_ADM_LANG = 'adminlanguage';
    const OPT_CP_STYLE = 'cpstyle';
    const OPT_PHP = 'php';
    
    private $group;
    private $name;
    private $title;
    private $description;
    private $optionscode;
    private $value;
    private $disporder;
    
    public function __construct(MyBBSettingsGroup $group, $name, $title, $description, $optionscode, $value, $disporder) {
        $this->name = $name;
        $this->title = $title;
        $this->description = $description;
        $this->optionscode = $optionscode;
        $this->value = $value;
        $this->disporder = $disporder;
        $this->group = $group;
    }

    public function name() {
        return $this->name;
    }

    public function title() {
        return $this->title;
    }

    public function description() {
        return $this->description;
    }

    public function optionsCode() {
        return $this->optionscode;
    }

    public function value() {
        return $this->value;
    }

    public function disporder() {
        return $this->disporder;
    }

    /**
     * @return MyBBSettingsGroup
     */
    public function group(){
        return $this->group;
    }
}
