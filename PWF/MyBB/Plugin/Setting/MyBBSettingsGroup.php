<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace PWF\MyBB\Plugin\Setting;

/**
 * 
 * @author vincent
 */
class MyBBSettingsGroup {
    private $id;
    private $name;
    private $title;
    private $description;
    private $order;
    
    public function __construct($id, $name, $title, $description, $order) {
        $this->id = $id;
        $this->name = $name;
        $this->title = $title;
        $this->description = $description;
        $this->order = $order;
    }

    public function id(){
        return $this->id;
    }
    
    public function name(){
        return $this->name;
    }
    
    public function title(){
        return $this->title;
    }
    
    public function description(){
        return $this->description;
    }
    
    public function order(){
        return $this->order;
    }
    
    public function fork($id){
        return new MyBBSettingsGroup(
            $id, 
            $this->name, 
            $this->title, 
            $this->description, 
            $this->order
        );
    }
}
