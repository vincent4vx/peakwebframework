<?php

/*
 * The MIT License
 *
 * Copyright 2016 vincent.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace PWF\MyBB\Plugin;

/**
 * Hooks system for MyBB
 *
 * @author vincent
 */
class MyBBHooks {
    /**
     * @var \pluginSystem
     */
    private $plugins;
    
    public function __construct(\pluginSystem $plugins) {
        $this->plugins = $plugins;
    }

    /**
     * Register a new hook
     * Can be use with closure or arrays
     * @param string $hook The hook name
     * @param callable $fn hook function to call. Can be a string (use standard MyBB system), or an over callable (closure, array, ...)
     * @param int $priority
     * @param string $file The filename to include when the hook is run
     * @return boolean
     */
    public function add($hook, callable $fn, $priority = 10, $file = ''){
        if(is_string($fn))
            return $this->plugins->add_hook($hook, $function, $priority, $file);
        
        if(!isset($this->plugins->hooks[$hook]))
            $this->plugins->hooks[$hook] = [];
        
        if(!isset($this->plugins->hooks[$hook][$priority]))
            $this->plugins->hooks[$hook][$priority] = [];
        
        $this->plugins->hooks[$hook][$priority][] = [
            'function' => $fn,
            'file' => $file
        ];
        
        return true;
    }
    
    /**
     * Run the hooks that have plugins.
     *
     * @param string The name of the hook that is run.
     * @param string The argument for the hook that is run. The passed value MUST be a variable
     * @return string The arguments for the hook.
     */
    public function run($hook, &$arg = ''){
        return $this->plugins->run_hooks($hook, $arg);
    }
    
    /**
     * Remove a specific hook.
     * @warning Don't run with closure or array functions. Only strings !
     * @param string The name of the hook.
     * @param string The function of the hook.
     * @param string The filename of the plugin.
     * @param int The priority of the hook.
     */
    public function remove($hook, $function, $file = '', $priority = 10){
        return $this->plugins->remove_hook($hook, $function, $file, $priority);
    }
}
