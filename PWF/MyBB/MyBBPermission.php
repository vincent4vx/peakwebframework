<?php

/*
 * The MIT License
 *
 * Copyright 2016 vincent.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace PWF\MyBB;

/**
 * Check permissions for MyBB apps
 * @author vincent
 */
class MyBBPermission {
    /**
     *
     * @var \session
     */
    private $session;
    
    /**
     * @var \MyBB
     */
    private $mybb;
    
    public function __construct(\session $session, \MyBB $mybb) {
        $this->session = $session;
        $this->mybb = $mybb;
    }
    
    /**
     * Check if the user is a guest
     */
    public function isGuest(){
        return $this->mybb->user['uid'] == 0;
    }
    
    /**
     * Check if the user is a super admin
     */
    public function isSuperAdmin(){
        if($this->isGuest())
            return false;
        
        return in_array($this->mybb->user['uid'], explode(',', $this->mybb->config['super_admins']));
    }

    /**
     * 
     * @param int|array $gid The group id or array of group id
     * @param bool $superAdmin If true, this method return always true with a superadmin
     * @return boolean
     */
    public function isMemberOfGroup($gid, $superAdmin = true){
        if($superAdmin && $this->isSuperAdmin())
            return true;
        
        $groups = empty($this->mybb->user['additionalgroups']) ? [] : explode(',', $this->mybb->user['additionalgroups']);
        array_unshift($groups, $this->mybb->user['usergroup']);
        
        foreach((array)$gid as $group){
            if(in_array($group, $groups))
                return true;
        }
        
        return false;
    }
}
