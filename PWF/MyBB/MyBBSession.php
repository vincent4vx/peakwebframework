<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace PWF\MyBB;

use PWF\Kernel;

require_once MYBB_ROOT . 'inc/class_session.php';

/**
 * Description of MyBBSession
 *
 * @author vincent
 */
class MyBBSession {
    /**
     * @var \session
     */
    private $session;
    
    public function __construct(Kernel $kernel, \MyBB $mybb){
        if(isset($mybb->session))
            $this->session = $mybb->session;
        else{
            $this->session = new \session();
            
        }
    }
    
    private function loadSession(){
        
    }
}
