<?php

/*
 * The MIT License
 *
 * Copyright 2016 vincent.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace PWF\Database\MyBB;

use PWF\MyBBKernel;

/**
 * Database system for MyBB apps
 *
 * @author vincent
 */
class MyBBDatabase {
    const TYPE_MYSQL = 'mysql';
    const TYPE_PGSQL = 'pgsql';
    const TYPE_SQLITE = 'sqlite';
    const TYPE_MYSQLi = 'mysqli';
    
    private $database;
    
    public function __construct(MyBBKernel $kernel) {
        $this->database = $kernel->mybb_db;
    }
    
    /**
     * Query the database.
     *
     * @param string The query SQL.
     * @param boolean 1 if hide errors, 0 if not.
     * @return resource The query data.
     */
    public function query($sql, $hide_errors = 0){
        return $this->database->query($sql, $hide_errors);
    }
    
    /**
     * Execute a write query on the slave database
     *
     * @param string The query SQL.
     * @param boolean 1 if hide errors, 0 if not.
     * @return resource The query data.
     */
    public function writeQuery($sql, $hide_errors = 0){
        return $this->database->write_query($sql, $hide_errors);
    }
    
    /**
     * Performs a simple select query.
     *
     * @param string The table name to be queried.
     * @param string Comma delimetered list of fields to be selected.
     * @param string SQL formatted list of conditions to be matched.
     * @param array List of options, order by, order direction, limit, limit start
     */
    public function simpleSelect($table, $fields = "*", $conditions = "", array $options = []){
        return $this->database->simple_select($table, $fields, $conditions, $options);
    }
    
    /**
     * Emulate prepare (escape values into $conditions) and perform a simpleSelect query
     * <code>
     * $conditions = [
     *      'title like "%{0}%"' => ["My terms"],
     *      'user IN("{0}", "{1}", "{2}")' => ["user1", "user2", "user3"],
     *      'content IS NOT NULL'
     * ];
     * $this->secureSelect('message', '*', $conditions);
     * //Generated query is : SELECT * FROM {prefix}message WHERE title like "%My terms%" AND user IN("user1", "user2", "user3") AND content IS NOT NULL
     * </code>
     * @param string $table The table name
     * @param string $fields The list of fields
     * @param array $conditions The where condition, each row are separated by AND
     * @param array $options List of options, order_by, order_direction, limit, limit_start
     * @return resource The statement resource
     * 
     * @see MyBBDatabase::simpleSelect()
     */
    public function secureSelect($table, $fields = '*', array $conditions = [], array $options = []){
        $cond = '';
        $b = false;
        
        foreach($conditions as $sql => $values){
            if($b)
                $cond .= ' AND ';
            else
                $b = true;
            
            if(!is_string($sql) && is_string($values)){
                $cond .= $values;
            }else{
                $values = (array)$values;
                
                foreach($values as $k => $v){
                    $sql = str_replace('{' . $k . '}', $this->escapeString($v), $sql);
                }
                
                $cond .= $sql;
            }
        }
        
        return $this->simpleSelect($table, $fields, $cond, $options);
    }
    
    /**
     * Return a result array for a query.
     *
     * @param resource The result data.
     * @param constant The type of array to return.
     * @return array The array of results.
     */
    public function fetchArray($r){
        return $this->database->fetch_array($r);
    }
    
    /**
     * Return a specific field from a query.
     *
     * @param resource The query ID.
     * @param string The name of the field to return.
     * @param int The number of the row to fetch it from.
     */    
    public function fetchField($r, $field, $row = false){
        return $this->database->fetch_field($r, $field, $row);
    }
    
    /**
     * Fetch all rows into an array
     * @param resource $r The statement resource
     */
    public function fetchAll($r){
        if($r instanceof \PDOStatement){
            return $r->fetchAll();
        }
        
        switch($this->database->engine){
            case 'pgsql':
                return pg_fetch_all($r);
            default:
                $arr = [];
                while(($row = $this->fetchArray($r)))
                    $arr[] = $row;
                return $arr;
        }
    }
    
    /**
     * Return the number of rows resulting from a query.
     *
     * @param resource The query data.
     * @return int The number of rows in the result.
     */
    public function numRows($r){
        return $this->database->num_rows($r);
    }
    
    /**
     * Return the last id number of inserted data.
     *
     * @return int The id number.
     */
    public function insertId($name = ''){
        return $this->database->insert_id($name);
    }
    
    /**
     * Build an insert query from an array.
     *
     * @param string The table name to perform the query on.
     * @param array An array of fields and their values.
     * @return int The insert ID if available
     */
    public function insertQuery($table, array $array){
        return $this->database->insert_query($table, $array);
    }
    
    /**
     * Build one query for multiple inserts from a multidimensional array.
     *
     * @param string The table name to perform the query on.
     * @param array An array of inserts.
     * @return int The insert ID if available
     */
    public function insertMultiple($table, array $array){
        return $this->database->insert_query_multiple($table, $array);
    }
    
    /**
     * Build an update query from an array.
     *
     * @param string The table name to perform the query on.
     * @param array An array of fields and their values.
     * @param string An optional where clause for the query.
     * @param string An optional limit clause for the query.
     * @param boolean An option to quote incoming values of the array.
     * @return resource The query data.
     */
    public function updateQuery($table, array $array, $where = '', $limit = '', $no_quote = false){
        return $this->database->update_query($table, $array, $where, $limit, $no_quote);
    }
    
    /**
     * Build a delete query.
     *
     * @param string The table name to perform the query on.
     * @param string An optional where clause for the query.
     * @param string An optional limit clause for the query.
     * @return resource The query data.
     */
    public function deleteQuery($table, $where = '', $limit = ''){
        return $this->database->delete_query($table, $where, $limit);
    }
    
    /**
     * Escape a string
     *
     * @param string The string to be escaped.
     * @return string The escaped string.
     */
    public function escapeString($str){
        return $this->database->escape_string($str);
    }
    
    /**
     * The type of db software being used.
     * @return string One of the constants MyBBDatabase::TYPE_
     */
    public function getType(){
        return $this->database->type;
    }
    
    /**
     * The table prefix used for simple select, update, insert and delete queries
     *
     * @return string
     */
    public function getTablePrefix(){
        return $this->database->table_prefix;
    }
    
    /**
     * Create a new table
     * @param string $table The table name
     * @param array $attrs Attributes list [attrName] => [attrDef]
     * @param array $indexes List of indexes
     */
    public function createTable($table, array $attrs, array $indexes = []){
        $query = 'CREATE TABLE `' . $this->getTablePrefix() . $this->escapeString($table) . '`(';
        
        if(empty($attrs))
            throw new \InvalidArgumentException('Attributes cannot be empty');
        
        $b = false;
        foreach($attrs as $attr => $def){
            if($b)
                $query .= ', ' . PHP_EOL;
            else
                $b = true;
            
            $query .= '`' . $this->escapeString($attr) . '` ' . $def;
        }
        
        foreach($indexes as $index){
            $query .= ', ' . PHP_EOL . $index;
        }
        
        $query .= ')';
        
        $this->query($query);
    }
    
    /**
     * Delete a table from database
     * @param string $table The table name
     */
    public function dropTable($table){
        $query = 'DROP TABLE IF EXISTS `' . $this->getTablePrefix() . $this->escapeString($table) . '`';
        $this->query($query);
    }
}
