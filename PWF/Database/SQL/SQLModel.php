<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace PWF\Database\SQL;

/**
 * Simple Model class for SQL database
 * @author vincent
 */
abstract class SQLModel{
    /**
     *
     * @var SQLDatabase
     */
    protected $db;
    
    public function __construct(\PWF\Database\DatabaseHandler $handler) {
        $this->db = $handler->getSQLConnection();
    }
}
