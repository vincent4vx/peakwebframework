<?php

/*
 * The MIT License
 *
 * Copyright 2015 Vincent Quatrevieux <quatrevieux.vincent@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace PWF\Helper;

/**
 * Description of Captcha
 *
 * @author Vincent Quatrevieux <quatrevieux.vincent@gmail.com>
 */
class Captcha {
    /**
     *
     * @var \PWF\Session\Session
     */
    private $session;
    
    const DEFAULT_CHARSET = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
    const DEFAULT_MIN_LEN = 4;
    const DEFAULT_MAX_LEN = 6;
    
    const DEFAULT_IMG_WIDTH  = 150;
    const DEFAULT_IMG_HEIGHT = 75;
    const DEFAULT_IMG_DEFORM = 3;
    const DEFAULT_IMG_BG     = '#FFF';
    
    const KEY_CHARSET = 'charset';
    const KEY_MIN_LEN = 'min_len';
    const KEY_MAX_LEN = 'max_len';
    
    const KEY_IMG_WIDTH  = 'width';
    const KEY_IMG_HEIGHT = 'height';
    const KEY_IMG_DEFORM = 'deform';
    const KEY_IMG_BG     = 'background';
    
    public function __construct(\PWF\Session\Session $session) {
        $this->session = $session;
    }

    public function generate(array $conf = []){
        $conf = array_replace([
            self::KEY_CHARSET => self::DEFAULT_CHARSET,
            self::KEY_MIN_LEN => self::DEFAULT_MIN_LEN,
            self::KEY_MAX_LEN => self::DEFAULT_MAX_LEN,
            self::KEY_IMG_WIDTH => self::DEFAULT_IMG_WIDTH,
            self::KEY_IMG_HEIGHT => self::DEFAULT_IMG_HEIGHT,
            self::KEY_IMG_DEFORM => self::DEFAULT_IMG_DEFORM,
            self::KEY_IMG_BG => self::DEFAULT_IMG_BG
        ], $conf);
        
        $text = '';
        $len = rand($conf[self::KEY_MIN_LEN], $conf[self::KEY_MAX_LEN]);
        $charset = $conf[self::KEY_CHARSET];
        $setlen = strlen($charset);
        
        for(;$len > 0; --$len){
            $text .= $charset{rand(0, $setlen - 1)};
        }
        
        $img = new Captcha\Image($text, $conf);
        
        $this->session->captcha = $img;
        
        return $img;
    }
    
    public function retrieve(array $conf = []){
        if(!isset($this->session->captcha))
            return $this->generate($conf);
        
        return $this->session->captcha;
    }
    
    public function validate($text){
        if(!isset($this->session->captcha))
            return false;
        
        return $this->session->captcha->isValid($text);
    }
    
    public function drop(){
        unset($this->session->captcha);
    }
}
