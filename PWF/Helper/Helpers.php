<?php

/*
 * The MIT License
 *
 * Copyright 2015 Vincent Quatrevieux <quatrevieux.vincent@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace PWF\Helper;

use PWF\Loader;
use PWF\Kernel;

/**
 * Handle helpers
 * @author Vincent Quatrevieux <quatrevieux.vincent@gmail.com>
 * 
 * @property-read Url $url
 * @property-read Assets $assets
 */
class Helpers {
    /**
     *
     * @var Loader
     */
    private $loader;
    
    private $ns;
    
    private $helpers = [];
    
    public function __construct(Kernel $kernel) {
        $this->loader = $kernel->loader;
        $this->ns = $kernel->conf('helpers.ns', []);
        $this->ns[] = __NAMESPACE__;
    }
    
    /**
     * Add a namespace for load helpers classes
     * @param string $ns The namespace, without the last backslash "\"
     */
    public function registerNamespace($ns){
        $this->ns[] = $ns;
    }
    
    public function __get($name) {
        $name = ucfirst($name);
        
        if(isset($this->helpers[$name]))
            return $this->helpers[$name];
        
        foreach($this->ns as $ns) {
            $className = $ns . '\\' . $name;
            
            if(!class_exists($className))
                continue;
            
            $this->helpers[$name] = $this->loader->getInstance($className);
            return $this->helpers[$name];
        }
        
        throw new \OutOfBoundsException('Helper ' . $name . ' not found');
    }
}
