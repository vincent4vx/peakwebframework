<?php

/*
 * The MIT License
 *
 * Copyright 2015 Vincent Quatrevieux <quatrevieux.vincent@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace PWF\Helper;

/**
 * 
 * @author Vincent Quatrevieux <quatrevieux.vincent@gmail.com>
 */
class Assets {
    /**
     *
     * @var Url
     */
    private $url;
    
    private $config;
    
    public function __construct(Url $url, \PWF\Kernel $kernel) {
        $this->config = $kernel->conf('assets', [
            'css_dir' => 'assets/css/',
            'img_dir' => 'assets/img/',
            'js_dir'  => 'assets/js/',
            'lib_dir' => 'assets/lib/'
        ]);
        
        $this->url = $url;
    }
    
    /*
     * Url generators
     */
    
    public function cssUrl($path){
        return $this->url->base() . $this->config['css_dir'] . $path . '.css';
    }
    
    public function imgUrl($path){
        return $this->url->base() . $this->config['img_dir'] . $path;
    }
    
    public function jsUrl($path){
        return $this->url->base() . $this->config['js_dir'] . $path . '.js';
    }
    
    public function libUrl($path){
        return $this->url->base() . $this->config['lib_dir'] . $path;
    }
    
    /*
     * CSS tags
     */
    
    public function cssBase($url){
        return '<link rel="stylesheet" href="' . $url . '"/>' . PHP_EOL;
    }
    
    public function css($file){
        return $this->cssBase($this->cssUrl($file));
    }
    
    public function cssLib($file){
        return $this->cssBase($this->libUrl($file));
    }
    
    /*
     * JS tags
     */
    
    public function jsBase($url){
        return '<script src="' . $url . '"></script>' . PHP_EOL;
    }
    
    public function js($file){
        return $this->jsBase($this->jsUrl($file));
    }
    
    public function jsLib($file){
        return $this->jsBase($this->libUrl($file));
    }
    
    /*
     * Img tags
     */
    
    public function imgBase($url, $alt, array $attrs = []){
        $tag = '<img src="' . htmlentities($url) . '" alt="' . htmlentities($alt) . '"';
        
        foreach ($attrs as $name => $value) {
            if(is_int($name))
                $tag .= ' ' . htmlentities($value);
            else
                $tag .= ' ' . htmlentities($name) . '="' . htmlentities($value) . '"';
        }
        
        $tag .= '/>';
        
        return $tag;
    }
    
    public function img($file, $alt, array $attrs = []){
        return $this->imgBase($this->imgUrl($file), $alt, $attrs);
    }
}
