<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace PWF\Helper;

/**
 * BBCode parser main class
 *
 * @author Vincent Quatrevieux <quatrevieux.vincent@gmail.com>
 */
class BBCode {
    private $tags = [];
    
    /**
     * Default start char of BB tag
     */
    const BBTAG_START = '[';
    
    /**
     * Default end char of BB tag
     */
    const BBTAG_END = ']';
    
    /**
     * Default close BB tag char
     */
    const BBTAG_CLOSE = '/';
    
    /**
     * Default char for start of value
     */
    const BBATTR_VALUE = '=';
    
    /**
     * Escaping char
     */
    const ESCAPE_CHAR = '\\';
    
    const STATE_DEFAULT     = 0;
    const STATE_ESCAPE      = 1;
    const STATE_IN_TAG      = 2;
    const STATE_CLOSING_TAG = 4;
    
    
    public function __construct(\PWF\Kernel $kernel) {
        foreach($kernel->conf('bbcode', [
            ['i', '<em>$contents</em>'],
            ['b', '<strong>$contents</strong>'],
            ['u', '<u>$contents</u>'],
            ['color', '<span style="color: $value;">$contents</span>'],
            ['align', '<div style="text-align: $value;">$contents</div>'],
            ['img', '<img src="$contents" alt="$value"/>'],
            ['br', '<br/>', true]
        ]) as $bbcode){
            call_user_func_array([$this, 'registerBBCode'], $bbcode);
        }
    }
    
    public function registerNewTag(BBTag $tag){
        $this->tags[strtolower($tag->getName())] = $tag;
    }
    
    public function registerBBCode($tag, $replace, $isSingle = false){
        $this->registerNewTag(new BBTag($tag, $replace, $isSingle));
    }
    
    public function checkState($curState, $state){
        return ($state & $curState) === $state;
    }
    
    public function remState(&$curState, $state){
        $curState &= ~($state);
    }
    
    public function parse($bbstring){
        $document = new BBElement(new BBTag('', '$contents'));
        $stack = new \SplStack();
        $stack->push($document);
        
        $contents = '';
        $state = self::STATE_DEFAULT;
        
        
        foreach(str_split($bbstring) as $char){
            if($this->checkState($state, self::STATE_ESCAPE)){
                $contents .= $char;
                $this->remState($state, self::STATE_ESCAPE);
                continue;
            }
            
            if($char === self::ESCAPE_CHAR){
                $state |= self::STATE_ESCAPE;
                continue;
            }
            
            if($this->checkState($state, self::STATE_IN_TAG)){
                if($char === self::BBTAG_END){
                    if($this->checkState($state, self::STATE_CLOSING_TAG)){
                        $this->closeTag($stack, $contents);
                        $this->remState($state, self::STATE_CLOSING_TAG);
                    }else{
                        $this->openTag($stack, $contents);
                    }

                    $contents = '';
                    $this->remState($state, self::STATE_IN_TAG);
                    continue;
                }elseif($char === self::BBTAG_CLOSE){
                    $state |= self::STATE_CLOSING_TAG;
                    continue;
                }
            }
            
            if($char === self::BBTAG_START){
                $state |= self::STATE_IN_TAG;
                
                if(!empty($contents))
                    $this->addTextContent($stack, $contents);
                
                $contents = '';
                continue;
            }
            
            $contents .= $char;
        }
        
        if(!empty($contents))
            $this->addTextContent($stack, $contents);
        
        return $document->parse();
    }
    
    private function openTag(\SplStack $stack, $contents){
        $parent = $stack->top();
        $data = explode(self::BBATTR_VALUE, $contents, 2);
        $tagName = $data[0];
        $tagAttr = (count($data) === 2) ? $data[1] : null;
        
        $tag = $this->_getTag($tagName);
        
        if($tag === null)
            return;
        
        $curElem = new BBElement($tag);
        
        if(!empty($tagAttr))
            $curElem->setValue($tagAttr);
        
        $parent->addContents($curElem);
        
        if(!$tag->isSingle())
            $stack->push($curElem);
    }
    
    private function closeTag(\SplStack $stack, $tagName){
        if(empty($tagName))
            return;
        
        $size = count($stack);
        
        for($i = 0; $i < $size; ++$i){
            $elem = $stack[$i];
            $curTag = strtolower($elem->getTag()->getName());
            
            if(empty($curTag))
                continue;
            
            if($curTag === strtolower($tagName)){
                unset($stack[$i]);
                break;
            }
        }
    }
    
    private function addTextContent(\SplStack $stack, $contents){
        $stack->top()->addContents(new BBTextElement($contents));
    }
    
    public function _getTag($name){
        $name = strtolower($name);
        
        if(!isset($this->tags[$name]))
            return null;
        
        return $this->tags[$name];
    }
    
    public function isBBTagStart($char){
        return self::BBTAG_START === $char;
    }
    
    public function isBBTagEnd($char){
        return self::BBTAG_END === $char;
    }
    
    public function isBBTagClose($char){
        return self::BBTAG_CLOSE === $char;
    }
}

class BBTag{
    private $name;
    private $parsing;
    private $single;
    
    function __construct($name, $parsing, $single = false) {
        $this->name = $name;
        $this->parsing = $parsing;
        $this->single = $single;
    }

    public function getName() {
        return $this->name;
    }

    public function isSingle() {
        return $this->single;
    }
    
    public function getParsing() {
        return $this->parsing;
    }
    
    public function toHTML($contents, $value){
        if(is_callable($this->parsing)){
            $callback = $this->parsing;
            return $callback($contents, $value);
        }
        
        return str_replace(
            array('$contents', '$value'), 
            array($contents, $value), 
            $this->getParsing()
        );
    }

}

class BBElement{
    /**
     *
     * @var BBTag
     */
    private $tag;
    private $contents = [];
    private $value;
    
    function __construct($tag) {
        $this->tag = $tag;
    }

    public function addContents(BBElement $contents){
        $this->contents[] = $contents;
    }
    
    public function getTag() {
        return $this->tag;
    }
    
    public function setValue($value) {
        $this->value = $value;
    }

    public function parse(){
        $contents = '';
        foreach($this->contents as $child){
            $contents .= $child->parse();
        }
        
        return $this->tag->toHTML($contents, $this->value);
    }
}

class BBTextElement extends BBElement{
    private $text;
    
    public function __construct($text) {
        parent::__construct(new BBTag('', ''));
        $this->text = $text;
    }

    public function parse($indent = 0) {
        return $this->text;
    }

}