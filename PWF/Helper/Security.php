<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace PWF\Helper;

use PWF\Input\InputStack;
use PWF\Output\InternalOutput;
use PWF\Output\OutputStack;

/**
 * Secutiry helper
 * @author vincent
 */
class Security {
    /**
     * @var InputStack
     */
    private $input;
    
    /**
     * @var OutputStack
     */
    private $output;
    
    public function __construct(InputStack $input, OutputStack $output) {
        $this->input = $input;
        $this->output = $output;
    }

    /**
     * Check if the request is internal or is HTTPS is enabled
     * @return bool true is is secure, or false
     */
    public function isSecure(){
        if($this->output->current() instanceof InternalOutput)
            return true;
        
        return isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] && $_SERVER['HTTPS'] != 'off';
    }
}
