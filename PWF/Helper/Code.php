<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace PWF\Helper;

/**
 * Simple code methods
 * to extends MyBB template system capabilities
 *
 * @author vincent
 */
class Code {
    public function ifVal($cond, $trueVal, $falseVal = null){
        return $cond ? $trueVal : $falseVal;
    }
    
    public function ifArrayContains($array, $val, $trueVal, $falseVal = null){
        if(!is_array($array))
            $array = (array)$array;
        
        return array_search($val, $array) !== false ? $trueVal : $falseVal;
    }
    
    public function forAll(array $array, callable $fn){
        $ret = '';
        
        foreach ($array as $k=>$v) {
            $ret .= $fn($k, $v);
        }
        
        return $ret;
    }
    
    public function date($format, $time){
        return date($format, $time);
    }
}
