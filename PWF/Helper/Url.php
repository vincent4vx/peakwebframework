<?php

/*
 * The MIT License
 *
 * Copyright 2015 Vincent Quatrevieux <quatrevieux.vincent@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace PWF\Helper;

use PWF\Input\HttpInput;

/**
 * Generate absolute url
 *
 * @author Vincent Quatrevieux <quatrevieux.vincent@gmail.com>
 */
class Url {
    private $rewrite;
    
    public function __construct(\PWF\Kernel $kernel) {
        $this->rewrite = $kernel->conf('url_rewriting', false);
    }
    
    /**
     * Get the base url of the website
     * @return string
     */
    public function base(){
        if(isset($_SERVER['HTTPS'])){
            $protocol = ($_SERVER['HTTPS'] && $_SERVER['HTTPS'] != "off") ? "https" : "http";
        }else{
            $protocol = 'http';
        }
        
        $dir = dirname($_SERVER['SCRIPT_NAME']);
        
        if(strlen($dir) > 0 && substr($dir, -1) != '/')
            $dir .= '/';
        
        return $protocol . "://" . $_SERVER['HTTP_HOST'] . $dir;
    }
    
    /**
     * Build an URL
     * @param string $path Path to webpage
     * @param array $get GET parameters
     * @return string The URL
     */
    public function url($path, array $get = []){
        $url = $this->base();
        
        if(!$this->rewrite)
            $url .= substr(strrchr($_SERVER['SCRIPT_NAME'], '/'), 1) . '/';
        
        if(is_array($path)){
            $url .= implode('/', $path);
        }else{
            $url .= $path;
        }
        
        //Remove the last /
        if($url{strlen($url) - 1} === '/')
            $url = substr($url, 0, -1);
        
        $query = http_build_query($get);
        
        if(empty($query))
            return $url;
        
        return $url . '?' . http_build_query($get);
    }
}

