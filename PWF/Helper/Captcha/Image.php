<?php

/*
 * The MIT License
 *
 * Copyright 2015 Vincent Quatrevieux <quatrevieux.vincent@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace PWF\Helper\Captcha;

use PWF\Helper\Captcha;

/**
 * Description of Image
 *
 * @author Vincent Quatrevieux <quatrevieux.vincent@gmail.com>
 */
class Image{
    private $width;
    private $height;
    private $text;
    private $img;
    
    private $gdImage;
    
    public function __construct($text, array $config) {
        $this->text = $text;
        $this->width = $config[Captcha::KEY_IMG_WIDTH];
        $this->height = $config[Captcha::KEY_IMG_HEIGHT];
        $this->gdImage = imagecreate($this->width, $this->height);
        
        $this->hex2color($config[Captcha::KEY_IMG_BG]);
        
        $this->generateImageText();
        
        if($config[Captcha::KEY_IMG_DEFORM] > 0)
            $this->deformImage($config[Captcha::KEY_IMG_DEFORM]);
        
        ob_start();
        imagejpeg($this->gdImage);
        $this->img = ob_get_clean();
        
        imagedestroy($this->gdImage);
        $this->gdImage = null;
    }
    
    private function hex2color($hex_color){
        $hex_color = substr($hex_color, 1);
        $rgb = [];
        
        foreach(str_split($hex_color, strlen($hex_color) === 6 ? 2 : 1) as $c){
            if(strlen($c) === 1)
                $c .= $c;
            $rgb[] = hexdec($c);
        }
        
        return imagecolorallocate($this->gdImage, $rgb[0], $rgb[1], $rgb[2]);
    }
    
    private function deformImage($poles){
        $poles_x = $poles_y = $poles_r = [];
        $img_d = sqrt($this->width * $this->width + $this->height * $this->height);
        
        for($i = 0; $i < $poles; $i++){
            $poles_x[] = mt_rand($this->width * .1, $this->width * .9);
            $poles_y[] = mt_rand($this->height * .1, $this->height * .9);
            $poles_r[] = mt_rand($img_d / 40, $img_d / 2);
        }
        
        $old_image = $this->gdImage;
        $this->gdImage = imagecreate($this->width, $this->height);
        imagepalettecopy($this->gdImage, $old_image);
        
        for($xi = 0; $xi < $this->width; $xi++){
            for($yi = 0; $yi < $this->height; $yi++){
                $pxl_x = $xi;
                $pxl_y = $yi;
                
                for($i = 0; $i < $poles; $i++){
                    $pole_x = $poles_x[$i];
                    $pole_y = $poles_y[$i];
                    $pole_r = $poles_r[$i];
                    
                    $dx = abs($pxl_x - $pole_x);
                    $dy = abs($pxl_y - $pole_y);
                    $d = sqrt($dx * $dx + $dy * $dy);
                    
                    if($d > $pole_r)
                        continue;
                    
                    $coef = sin(pi() * $d / $pole_r) * .1;
                    $pxl_x += ($pole_x - $pxl_x) * $coef;
                    $pxl_y += ($pole_y - $pxl_y) * $coef;
                }
                
                imagesetpixel($this->gdImage, $pxl_x, $pxl_y, imagecolorat($old_image, $xi, $yi));
            }
        }
        
        imagedestroy($old_image);
    }
    
    private function generateImageText(){
        $interval_size = $this->width / strlen($this->text) - 1;
        $x = 0;
        foreach(str_split($this->text) as $chr){
            $chr_x = mt_rand($x + $interval_size * .4, $x + $interval_size * .8);
            $chr_y = mt_rand($this->height * .4, $this->height * .8);
            
            $r = mt_rand(0, 255);
            $g = mt_rand(0, 255);
            $b = mt_rand(0, 255);
            $color = imagecolorallocate($this->gdImage, $r, $g, $b);
            imagettftext($this->gdImage, rand(18, 26), rand(0, 45), $chr_x, $chr_y, $color, __DIR__.'/police.ttf', $chr);
            $x += $interval_size;
        }
    }
    
    public function __sleep() {
        return ['width', 'height', 'img', 'text'];
    }
    
    public function show(\PWF\Output\Output $output){
        $output->setMimeType('image/jpeg');
        $output->setBody($this->img);
    }
    
    public function isValid($text){
        return strtolower($this->text) === strtolower($text);
    }
}
