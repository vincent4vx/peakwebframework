<?php

/*
 * The MIT License
 *
 * Copyright 2016 vincent.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace PWF\Helper;

use PWF\Input\HttpInput;

/**
 * Help for HTML forms
 *
 * @author vincent
 */
class Form {
    /**
     * @var HttpInput
     */
    private $input;
    
    public function __construct(HttpInput $input) {
        $this->input = $input;
    }

    public function checkbox($name, $value, array $attributes = []){
        $selected = false;
        if(Strings::endWith($name, '[]')){
            $selected = in_array($value, (array)$this->input->get(substr($name, 0, -2)));
        }else{
            $selected = $value == $this->input->get($name);
        }
        
        if($selected)
            $attributes[] = 'checked';
        
        return $this->input('checkbox', $name, $value, $attributes);
    }
    
    public function textBase($type, $name, array $attributes = []){
        $value = $this->input->get($name);
        
        if(!is_string($value))
            $value = '';
        
        return $this->input($type, $name, $value, $attributes);
    }
    
    public function text($name, array $attributes = []){
        return $this->textBase('text', $name, $attributes);
    }
    
    public function password($name, array $attributes = []){
        return $this->textBase('password', $name, $attributes);
    }
    
    public function input($type, $name, $value, array $attributes = []){
        $html = '<input type="' . htmlentities($type) . '" name="' . htmlentities($name) . '" value="' . htmlentities($value) . '" ';
        
        foreach($attributes as $name => $value){
            if(is_int($name)){
                $html .= htmlentities($value) . ' ';
            }else{
                $html .= htmlentities($name) . '="' . htmlentities($value) . '" ';
            }
        }
        
        $html .= '/>';
        return $html;
    }
}
