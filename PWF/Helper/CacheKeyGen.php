<?php

/*
 * The MIT License
 *
 * Copyright 2016 vincent.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace PWF\Helper;

use PWF\Input\HttpInput;

/**
 * Description of CacheKeyGen
 *
 * @author vincent
 */
class CacheKeyGen {
    /**
     *
     * @var HttpInput
     */
    private $input;
    
    private $usePath, $get, $post;
    
    public function __construct(HttpInput $input) {
        $this->input = $input;
    }

    /**
     * Use the path to generate the key
     * @return CacheKeyGen
     */
    public function usePath(){
        $this->usePath = true;
        return $this;
    }
    
    /**
     * Use GET vars
     * @param array $vars
     * @return CacheKeyGen
     */
    public function useGet(array $vars){
        $this->get = $vars;
        return $this;
    }
    
    /**
     * 
     * @param array $vars
     * @return \PWF\Helper\CacheKeyGen
     */
    public function usePost(array $vars){
        $this->post = $vars;
        return $this;
    }
    
    private function genParams(array $data, array $filter){
        $str = '';
        foreach($filter as $name => $value){
            if(is_numeric($name)){
                $name = $value;
                $value = null;
            }
            
            if(isset($data[$name]))
                $value = $data[$name];
            
            $str .= '&' . $name . '=' . $value;
        }
        
        return $str;
    }
    
    public function genKey(){
        $key = '';
        
        if($this->usePath)
            $key .= implode (':', $this->input->path());
        
        if($this->get){
            $key .= '_GET:' . $this->genParams($this->input->get(), $this->get);
        }
        
        if($this->post){
            $key .= '_POST:' . $this->genParams($this->input->get(), $this->get);
        }
        
        return $key;
    }
}
