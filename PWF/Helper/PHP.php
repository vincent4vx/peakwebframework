<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace PWF\Helper;

/**
 * Description of PHP
 *
 * @author vincent
 */
class PHP {
    static public function safeInclude($__file, array $__vars = []){
        extract($__vars, EXTR_SKIP);
        return include $__file;
    }
    
    static public function safeEval($code, array $__vars = []){
        $file = tempnam(sys_get_temp_dir(), 'php_eval');
        file_put_contents($file, '<?php ' . $code);
        $ret = self::safeInclude($file, $__vars);
        //Don't use finally, to keep file for debugging
        unlink($file);
        return $ret;
    }
}
