<?php

/*
 * The MIT License
 *
 * Copyright 2016 vincent.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace PWF\Helper;

use ReflectionClass;
use ReflectionFunctionAbstract;
use ReflectionProperty;

/**
 * Parser for php doc comment
 * @author vincent
 */
class PhpDocParser {
    private $tagClasses = [];
    
    public function __construct() {
        $this->registerDefaultTagClasses();
    }
    
    private function registerDefaultTagClasses(){
        $this->addPhpDocTag(PhpDocParam::class);
        $this->addPhpDocTag(PhpDocReturn::class);
    }
    
    /**
     * Register a new PhpDocTag class
     * @param string $className The class name of an implementation of PhpDocTag
     */
    public function addPhpDocTag($className){
        $this->tagClasses[$className::tagName()] = $className;
    }
    
    /**
     * Parse a Php doc comment
     * @param string|ReflectionClass|ReflectionFunctionAbstract|ReflectionProperty $data
     * @return PhpDocComment
     */
    public function parse($data){
        if($data instanceof ReflectionFunctionAbstract
                || $data instanceof ReflectionClass
                || $data instanceof ReflectionProperty){
            $data = $data->getDocComment();
        }
        
        $description = '';
        
        $comment = new PhpDocComment();
        
        foreach(explode(PHP_EOL, $data) as $line){
            $line = ltrim($line, ' \t\n\r\0\x0B/*');
            $line = rtrim($line);
            
            if(empty($line))
                continue;
            
            if($line{0} === '@'){
                $tag = $this->parseTagLine($line);
                
                if($tag)
                    $comment->addTag($tag);
            }else{
                $description .= $line . PHP_EOL;
            }
        }
        
        $comment->setDescription($description);
        
        return $comment;
    }
    
    private function parseTagLine($line){
        $tagName = explode(' ', substr($line, 1))[0];
        
        if(!isset($this->tagClasses[$tagName]))
            return null;
        
        $class = $this->tagClasses[$tagName];
        
        return $class::parse($line);
    }
}

/**
 * Doc comment class
 */
class PhpDocComment{
    private $tags = [];
    private $description;
    
    /**
     * Get all tags as array
     * <code>
     *  [
     *      [tagName] => [
     *          [Tag1],
     *          ...
     *      ],
     *      ...
     *  ]
     * </code>
     * @return array
     */
    public function getAllTags() {
        return $this->tags;
    }

    /**
     * Get the description
     * @return string
     */
    public function getDescription() {
        return $this->description;
    }

    public function setDescription($description) {
        $this->description = $description;
    }
    
    /**
     * Get all tags by name
     * @return PhpDocTag[]
     */
    public function getByTagName($tagName){
        return isset($this->tags[$tagName]) ? $this->tags[$tagName] : [];
    }
    
    /**
     * Alias of getByTagName
     * @param string $name tag name
     * @return PhpDocTag[]
     * @see PhpDocComment::getByTagName(string)
     */
    public function __get($name) {
        return $this->getByTagName($name);
    }
    
    /**
     * Check if a tag is present
     * @param string $name The tag name
     * @return bool
     */
    public function __isset($name) {
        return isset($this->tags[$name]);
    }
    
    /**
     * Add a new tag into the doc comment
     * @param PhpDocTag $tag
     */
    public function addTag(PhpDocTag $tag){
        if(empty($this->tags[$tag->tagName()]))
            $this->tags[$tag->tagName()] = [];
        
        $this->tags[$tag->tagName()][] = $tag;
    }
}

interface PhpDocTag{
    /**
     * Parse a line
     * @param string $tagLine The tag line
     * @return static
     */
    static public function parse($tagLine);
    
    /**
     * Get the tag name
     * @return string
     */
    static public function tagName();
}

class PhpDocParam implements PhpDocTag{
    private $type;
    private $name;
    private $description;
    
    public function __construct($type, $name, $description = '') {
        $this->type = $type;
        $this->name = $name;
        $this->description = $description;
    }

    public static function parse($tagLine) {
        $data = explode(' ', $tagLine, 4);
        
        if(count($data) < 3)
            return null;
        
        return new self(
            $data[1],
            $data[2],
            isset($data[3]) ? $data[3] : ''
        );
    }

    public static function tagName() {
        return 'param';
    }
    
    public function getName() {
        return $this->name;
    }

    public function getDescription() {
        return $this->description;
    }
    
    public function getType() {
        return $this->type;
    }
}

class PhpDocReturn implements PhpDocTag{
    private $type;
    private $description;
    
    public function __construct($type, $description) {
        $this->type = $type;
        $this->description = $description;
    }

    public static function parse($tagLine) {
        $data = explode(' ', $tagLine, 3);
        
        if(count($data) < 2)
            return null;
        
        return new self(
            $data[1], 
            isset($data[2]) ? $data[2] : ''
        );
    }

    public static function tagName() {
        return 'return';
    }
    
    public function getType() {
        return $this->type;
    }

    public function getDescription() {
        return $this->description;
    }
}