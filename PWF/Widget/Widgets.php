<?php

/*
 * The MIT License
 *
 * Copyright 2015 Vincent Quatrevieux <quatrevieux.vincent@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace PWF\Widget;

/**
 * Handle Widgets
 *
 * @author Vincent Quatrevieux <quatrevieux.vincent@gmail.com>
 */
class Widgets {
    private $widgets = [];
    
    /**
     *
     * @var \PWF\Loader
     */
    private $loader;
    
    private $ns;

    public function __construct(\PWF\Kernel $kernel, \PWF\Loader $loader) {
        $this->loader = $loader;
        $this->ns = $kernel->conf('widgets.ns', []);
        $this->ns[] = __NAMESPACE__;
    }
    
    public function addNamespace($ns){
        array_unshift($this->ns, $ns);
    }

    /**
     * Load a Widget
     * @param string $widget The widget name
     * @return Widget
     * @throws \OutOfBoundsException When widget is not found
     * @throws \InvalidArgumentException When widget class don't implements Widget
     */
    public function load($widget){
        $widget = ucfirst($widget);
        
        if(isset($this->widgets[$widget]))
            return $this->widgets[$widget];
        
        foreach($this->ns as $ns) {
            $class = $ns . '\\' . $widget;
            
            if(!class_exists($class))
                continue;
            
            $obj = $this->loader->getInstance($class);
            
            if(!($obj instanceof Widget))
                throw new \InvalidArgumentException($class . ' is not a valid widget');
                
            
            return $obj;
        }
        
        throw new \OutOfBoundsException('Widget ' . $widget . ' not found');
    }
    
    public function show($widget, array $vars = []){
        return $this->load($widget)->show($vars);
    }
}
