<?php

/*
 * The MIT License
 *
 * Copyright 2016 vincent.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace PWF\Input;

/**
 * Input for internal request (basic HVMC)
 * @author vincent
 */
class InternalInput implements HttpInput{
    private $script  = '/index.php';
    private $headers = [];
    private $cookies = [];
    private $get     = [];
    private $post    = [];
    private $input   = [];
    private $method  = HttpInput::METHOD_GET;
    private $path    = [];
    
    public function clientIpAddress() {
        return '127.0.0.1'; //Always local
    }

    public function cookie($name = null) {
        if($name === null)
            return $this->cookies;
        
        return isset($this->cookies[$name]) ? $this->cookies[$name] : null;
    }

    public function get($name = null) {
        if($name === null)
            return $this->get;
        
        return isset($this->get[$name]) ? $this->get[$name] : null;
    }

    public function header($name) {
        return isset($this->headers[$name]) ? $this->headers[$name] : null;
    }

    public function input($name = null) {
        if($name === null)
            return $this->input;
        
        return isset($this->input[$name]) ? $this->input[$name] : null;
    }

    public function method() {
        return $this->method;
    }

    public function path() {
        return $this->path;
    }

    public function post($name = null) {
        if($name === null)
            return $this->post;
        
        return isset($this->post[$name]) ? $this->post[$name] : null;
    }
    
    /**
     * 
     * @param string $name
     * @param string $value
     * @return InternalInput
     */
    public function setHeader($name, $value) {
        $this->headers[$name] = $value;
        return $this;
    }

    /**
     * 
     * @param string $name
     * @param string $value
     * @return InternalInput
     */
    public function setCookie($name, $value) {
        $this->cookies[$name] = $value;
        return $this;
    }

    /**
     * Set GET parameters
     * @param array|string $name The GET var name, or array with [key] => [value], to set a list
     * @param mixed|null $value The value, or null if $name if an array
     * @return InternalInput
     */
    public function setGet($name, $value = null) {
        if(is_array($name)){
            foreach($name as $k=>$v) {
                $this->get[$k] = $v;
            }
        }else{
            $this->get[$name] = $value;
        }
        return $this;
    }

    /**
     * 
     * @param string $name
     * @param string $value
     * @return InternalInput
     */
    public function setPost($name, $value) {
        $this->post[$name] = $value;
        return $this;
    }

    /**
     * @param mixed $input
     * @return InternalInput
     */
    public function setInput($input) {
        $this->input = $input;
        return $this;
    }

    /**
     * @param string $method
     * @return InternalInput
     */
    public function setMethod($method) {
        $this->method = $method;
        return $this;
    }

    /**
     * 
     * @param string|array $path
     * @return InternalInput
     */
    public function setPath($path) {
        if(!is_array($path))
            $path = explode('/', $path);
        
        $this->path = $path;
        return $this;
    }
    
    public function scriptName() {
        return $this->script;
    }
    
    /**
     * Set the script file name
     * @param string $script
     * @return InternalInput
     */
    public function setScript($script) {
        $this->script = $script;
        return $this;
    }
}
