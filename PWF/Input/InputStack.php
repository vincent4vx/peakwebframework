<?php

/*
 * The MIT License
 *
 * Copyright 2016 vincent.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace PWF\Input;

/**
 * Stack of HttpInput instances
 * @author vincent
 */
class InputStack implements HttpInput{
    /**
     * @var \SplStack
     */
    private $stack;
    
    public function __construct(HttpInput $appInput) {
        $this->stack = new \SplStack();
        $this->stack->push($appInput);
    }
    
    /**
     * Push a new HttpInput instance into the stack
     * @param HttpInput $input
     */
    public function push(HttpInput $input){
        $this->stack->push($input);
    }
    
    /**
     * Remove the current instance of HttpInput
     * @return HttpInput
     * @throws \UnderflowException When try to pop the app input instance (The current is the bottom element)
     */
    public function pop(){
        if($this->stack->bottom() === $this->stack->top())
            throw new \UnderflowException('Cannot pop the app input instance');
            
        return $this->stack->pop();
    }
    
    /**
     * Get the current HttpInput
     * @return HttpInput
     */
    public function current(){
        return $this->stack->top();
    }
    
    //Delegates methods
    
    public function clientIpAddress() {
        return $this->current()->clientIpAddress();
    }

    public function cookie($name = null) {
        return $this->current()->cookie($name);
    }

    public function get($name = null) {
        return $this->current()->get($name);
    }

    public function header($name) {
        return $this->current()->header($name);
    }

    public function input($name = null) {
        return $this->current()->input($name);
    }

    public function method() {
        return $this->current()->method();
    }

    public function path() {
        return $this->current()->path();
    }

    public function post($name = null) {
        return $this->current()->post($name);
    }

    public function scriptName() {
        return $this->current()->scriptName();
    }

}
