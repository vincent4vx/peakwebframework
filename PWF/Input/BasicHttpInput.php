<?php

/*
 * The MIT License
 *
 * Copyright 2015 Vincent Quatrevieux <quatrevieux.vincent@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace PWF\Input;

/**
 * Basic implementation of HttpInput
 *
 * @author Vincent Quatrevieux <quatrevieux.vincent@gmail.com>
 */
class BasicHttpInput implements HttpInput{
    private $cookies;
    private $get;
    private $post;
    private $path = null;
    private $input = null;

    public function __construct() {
        $this->cookies = $_COOKIE;
        $this->get = $_GET;
        $this->post = $_POST;
        
        array_walk_recursive($this->cookies, function(&$value){
            $value = $this->cleanCOOKIE($value);
        });
        
        array_walk_recursive($this->get, function(&$value){
            $value = $this->cleanGET($value);
        });
        
        array_walk_recursive($this->post, function(&$value){
            $value = $this->cleanPOST($value);
        });
    }
    
    protected function cleanGET($value){
        return trim(filter_var($value, FILTER_SANITIZE_STRING));
    }
    
    protected function cleanPOST($value){
        return trim($value);
    }
    
    protected function cleanCOOKIE($value){
        return $value;
    }

    public function cookie($name = null) {
        if($name === null)
            return $this->cookies;
        
        return isset($this->cookies[$name]) ? $this->cookies[$name] : null;
    }

    public function get($name = null) {
        if($name === null)
            return $this->get;
        
        return isset($this->get[$name]) ? $this->get[$name] : null;
    }

    public function header($name) {
        $name = 'HTTP_' . str_replace('-', '_', strtoupper($name));
        
        return isset($_SERVER[$name]) ? $_SERVER[$name] : null;
    }

    public function method() {
        return strtoupper($_SERVER['REQUEST_METHOD']);
    }

    public function path() {
        if($this->path === null){
            $this->path = [];
            
            if(!empty($_SERVER['PATH_INFO'])){
                foreach(explode('/', $_SERVER['PATH_INFO']) as $s){
                    $s = $this->cleanGET($s);

                    if(strlen($s) > 0)
                        $this->path[] = $s;
                }
            }
        }
        
        return $this->path;
    }

    public function post($name = null) {
        if($name === null)
            return $this->post;
        
        return isset($this->post[$name]) ? $this->post[$name] : null;
    }
    
    public function input($name = null) {
        if($this->input === null){
            $this->input = [];
            parse_str(file_get_contents('php://input'), $this->input);
        }
        
        if($name === null)
            return $this->input;
        
        return isset($this->input[$name]) ? $this->input[$name] : null;
    }

    public function clientIpAddress() {
        return $_SERVER['REMOTE_ADDR'];
    }
    
    public function scriptName() {
        return $_SERVER['SCRIPT_NAME'];
    }
}
