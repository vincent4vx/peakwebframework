<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace PWF\Input;

/**
 * Input class for CLI apps
 * @author vincent
 */
class CLIInput implements HttpInput{
    private $args = [];
    private $params = [];
    
    public function __construct() {
        $this->parseArgs();
    }
    
    private function parseArgs(){
        global $argc, $argv;
        
        for($i = 1; $i < $argc; ++$i){
            $arg = $argv[$i];
            
            if($arg{0} === '-'){
                $this->parseArg($arg);
            }else{
                $this->params[] = $arg;
            }
        }
    }
    
    private function parseArg($arg){
        $arg = explode('=', substr($arg, 1));
        
        if(substr($arg[0], -2) === '[]'){
            $name = substr($arg[0], 0, strlen($arg[0]) - 2);
            
            if(!isset($this->args[$name]))
                $this->args[$name] = [];
            
            $this->args[$name][] = $arg[1];
        }else{
            $this->args[$arg[0]] = $arg[1];
        }
    }
    
    public function clientIpAddress() {
        return '127.0.0.1'; //always on local
    }

    public function cookie($name = null) {
        return null;
    }

    public function get($name = null) {
        if($name === null)
            return $this->args;
        
        return isset($this->args[$name]) ? $this->args[$name] : null;
    }

    public function header($name) {
        
    }

    public function input($name = null) {
        
    }

    public function method() {
        
    }

    public function path() {
        return $this->params;
    }

    public function post($name = null) {
        
    }
    
    public function scriptName() {
        return $_SERVER['SCRIPT_NAME'];
    }
}
