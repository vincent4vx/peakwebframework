<?php

/*
 * The MIT License
 *
 * Copyright 2015 Vincent Quatrevieux <quatrevieux.vincent@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace PWF\Input\Validator\Rule;

/**
 * Description of Length
 *
 * @author Vincent Quatrevieux <quatrevieux.vincent@gmail.com>
 */
class Length implements ValidatorRule{
    private $min, $max;
    
    public function __construct($min, $max = null) {
        $this->min = $min;
        $this->max = $max;
    }

    public function getError() {
        if($this->min !== null && $this->max !== null){
            return 'Le champ doit contenir entre ' . $this->min . ' et ' . $this->max . ' caractères';
        }elseif($this->min !== null){
            return 'Le champ doit faire au moins ' . $this->min . ' caractères';
        }elseif($this->max !== null){
            return 'Le champ ne doit pas dépasser ' . $this->max . ' caractères';
        }
    }

    public function validate($value, $index = null) {
        $len = strlen($value);
        
        if($this->min !== null && $len < $this->min)
            return false;
        
        if($this->max !== null && $len > $this->max)
            return false;
        
        return true;
    }
}
