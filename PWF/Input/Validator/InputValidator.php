<?php

/*
 * The MIT License
 *
 * Copyright 2015 Vincent Quatrevieux <quatrevieux.vincent@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace PWF\Input\Validator;

use PWF\Output\HttpMethodNotAllowed;
use PWF\Input\HttpInput;

/**
 * Validate input
 * @author Vincent Quatrevieux <quatrevieux.vincent@gmail.com>
 */
class InputValidator {
    private $method;
    private $rules = [];
    private $custom = [];
    
    /**
     *
     * @var \PWF\Input\HttpInput
     */
    private $input;
    
    public function __construct(HttpInput $input) {
        $this->input = $input;
        $this->method = $input->method();
    }
    
    public function setMethod($method){
        $this->method = $method;
    }

    public function validate(){
        if($this->method !== $this->input->method()){
            throw new HttpMethodNotAllowed('Method ' . $this->method . ' is required');
        }
        
        $isValid = true;
        $errors = [];
        
        foreach($this->rules as $field => $rules) {
            $rawValue = $this->getValue($field);
            
            if($rawValue === null)
                $rawValue = '';
            
            foreach((array)$rawValue as $index => $value){
                $fieldName = $field;

                if(is_array($rawValue))
                    $fieldName .= '[' . $index . ']';

                foreach($rules as $rule) {
                    if(!$rule->validate($value, $index)){
                        $isValid = false;
                        
                        $errors[$fieldName] = [
                            'valid' => false,
                            'message' => $rule->getError()
                        ];
                        break;
                    }
                }
            
                if(!isset($errors[$fieldName])){
                    $errors[$fieldName] = [
                        'valid' => true
                    ];
                }
            }
        }
        
        foreach($this->custom as $custom){
            if(!$custom($errors))
                $isValid = false;
        }
        
        if(!$isValid)
            throw new InvalidInput($errors);
    }
    
    protected function getValue($name){
        switch($this->method){
            case HttpInput::METHOD_GET:
                return $this->input->get($name);
            case HttpInput::METHOD_POST:
                return $this->input->post($name);
            default:
                return $this->input->input($name);
        }
    }
    
    public function addRule($field, Rule\ValidatorRule $rule, $customError = null){
        if(!isset($this->rules[$field]))
            $this->rules[$field] = [];
        
        if($customError !== null)
            $rule = new Rule\CustomMessage($rule, $customError);
        
        $this->rules[$field][] = $rule;
    }
    
    public function equals($field, $value, $customError = null){
        $this->addRule($field, new Rule\Equals($value), $customError);
    }
    
    public function equalsCI($field, $value, $customError = null){
        $this->addRule($field, new Rule\Equals($value, false), $customError);
    }
    
    public function notEquals($field, $value, $customError = null){
        $this->addRule($field, new Rule\NotEquals($value), $customError);
    }
    
    public function notEqualsCI($field, $value, $customError = null){
        $this->addRule($field, new Rule\NotEquals($value, false), $customError);
    }
    
    public function filterVar($field, $filter, $customError = null){
        $this->addRule($field, new Rule\FilterVar($filter), $customError);
    }
    
    public function required($field, $customError = null){
        foreach((array)$field as $f)
            $this->addRule($f, Rule\Required::getInstance(), $customError);
    }
    
    public function regex($field, $regex, $customError = null){
        $this->addRule($field, new Rule\Regex($regex), $customError);
    }
    
    public function callback($field, callable $callback){
        $this->addRule($field, new Rule\Callback($callback));
    }
    
    public function length($field, $min, $max = null, $customError = null){
        $this->addRule($field, new Rule\Length($min, $max), $customError);
    }
    
    public function addCustomValidator(callable $callback){
        $this->custom[] = $callback;
    }
}
