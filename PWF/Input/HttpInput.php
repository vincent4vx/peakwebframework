<?php

/*
 * The MIT License
 *
 * Copyright 2015 Vincent Quatrevieux <quatrevieux.vincent@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace PWF\Input;

/**
 * Http input parameters
 * @author Vincent Quatrevieux <quatrevieux.vincent@gmail.com>
 */
interface HttpInput {
    const METHOD_GET     = 'GET';
    const METHOD_POST    = 'POST';
    const METHOD_PUT     = 'PUT';
    const METHOD_OPTIONS = 'OPTIONS';
    const METHOD_DELETE  = 'DELETE';
    
    /**
     * Get value of GET parameter
     * @param string $name The name of GET parameter or null to get all GET parameters
     * @return mixed The value or null
     */
    public function get($name = null);
    
    /**
     * Get value of POST parameter
     * @param string $name The name of POST parameter or null to get all POST parameters
     * @return mixed The value or null
     */    
    public function post($name = null);
    
    /**
     * Get value of php://input file (the input should encoded in form-encode)
     * @param string $name The name of the input
     * @return mixed The value or null
     */
    public function input($name = null);
    
    /**
     * Get value of a cookie
     * @param string $name the name of the cookie or null to get all cookies
     * @return mixed The value or null
     */
    public function cookie($name = null);
    
    /**
     * Get the HTTP request method
     * @return string One of the constants HttpInput::METHOD_*
     */
    public function method();
    
    /**
     * Get the value of an header
     * @param string $name The header name
     * @return string|null The header value or null
     */
    public function header($name);
    
    /**
     * Get the path of the request
     * @return string[] The path
     */
    public function path();
    
    /**
     * Get the IP Address of the client
     * @return string
     */
    public function clientIpAddress();
    
    /**
     * Get the script file name
     * @return string The relative path of the script file name from web root, start with /
     */
    public function scriptName();
}
