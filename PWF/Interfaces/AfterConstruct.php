<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace PWF\Interfaces;

use PWF\Loader;

/**
 * Function call after Loader contruction
 * @author vincent
 */
interface AfterConstruct {
    /**
     * Call this function after the constructor call
     * @param Loader $loader
     */
    public function afterConstruct(Loader $loader);
}
