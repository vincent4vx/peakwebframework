<?php

/*
 * The MIT License
 *
 * Copyright 2016 vincent.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace PWF\Router;

use PWF\Input\HttpInput;
use PWF\Kernel;
use PWF\Output\HttpNotFound;

/**
 * Description of RestRouter
 *
 * @author vincent
 */
class RestRouter implements Router{
    private $config;
    
    public function __construct(Kernel $kernel) {
        $app = $kernel->conf('app', [
            'controller_ns' => 'App\Controller',
            'contoller_suffix' => '',
            'action_suffix' => 'Action'
        ]);
        
        $router = $kernel->conf('router', [
            'default_controller' => 'home',
            'http_method_prefix' => 'http'
        ]);
        
        $this->config = array_merge($app, $router);
    }
    
    protected function processStartPath(array &$path, HttpInput $input, array $config){}
    
    protected function processController(array &$path, array $conf){
        $ctrl = array_shift($path);
        
        if(!$ctrl)
            $ctrl = $conf['default_controller'];
        
        return $conf['controller_ns'] . '\\' . ucfirst($ctrl) . $conf['controller_suffix'];
    }
    
    public function getRoute(HttpInput $input) {
        $config = $this->getConfig($input);
        
        $path = $input->path();
        $this->processStartPath($path, $input, $config);
        
        $controller = $this->processController($path, $config);
        
        if(!class_exists($controller))
            throw new HttpNotFound('Resource not found');
        
        if(isset($path[0])){
            $action = array_shift($path) . $config['action_suffix'];
            
            if(!method_exists($controller, $action))
                $action = false;
        }
        
        if(!$action){
            $action = $config['http_method_prefix'] . strtoupper($input->method());
        }
        
        $parameters = $path;
        
        return new RouteImpl($controller, $action, $parameters);
    }
    
    public function getConfig(HttpInput $input){
        return array_merge($this->config, $this->config[$input->scriptName()]);
    }
}
