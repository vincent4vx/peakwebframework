<?php

/*
 * The MIT License
 *
 * Copyright 2016 vincent.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace PWF\Router;

use PWF\Input\HttpInput;
use PWF\Kernel;

/**
 * Base class for http path router
 *
 * @author vincent
 */
abstract class AbstractHttpPathRouter implements Router{
    protected $config;
    
    public function __construct(Kernel $kernel) {
        $app = $kernel->conf('app', [
            'controller_ns' => 'App\Controller',
            'controller_suffix' => '',
            'action_suffix' => 'Action'
        ]);
        
        $router = $kernel->conf('router', [
            'default_controller' => 'home',
            'default_action' => 'index'
        ]);
        
        $this->config = array_merge($app, $router);
    }
    
    protected function processPathStart(array &$path, array $conf){}
    
    protected function processController(array &$path, array $conf){
        $ctrl = array_shift($path);
        
        if(!$ctrl)
            $ctrl = $conf['default_controller'];
        
        return $conf['controller_ns'] . '\\' . ucfirst($ctrl) . $conf['controller_suffix'];
    }
    
    protected function processAction(array &$path, array $conf){
        $action = array_shift($path);
        
        if(!$action)
            $action = $conf['default_action'];
        
        return $action . $conf['action_suffix'];
    }
    
    protected function processParameters(array &$path, array $conf){
        return $path;
    }

    public function getRoute(HttpInput $input) {
        $conf = $this->config;
        
        if(isset($this->config[$input->scriptName()])){
            $conf = array_merge($conf, $this->config[$input->scriptName()]);
        }
        
        $path = $input->path();
        
        $this->processPathStart($path, $conf);
        $controller = $this->processController($path, $conf);
        $action = $this->processAction($path, $conf);
        $parameters = $this->processParameters($path, $conf);
        
        return new RouteImpl(
            $controller, 
            $action, 
            $parameters
        );
    }
}
