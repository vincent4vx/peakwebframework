<?php

/*
 * The MIT License
 *
 * Copyright 2016 vincent.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace PWF\Router;

use PWF\Constants;
use PWF\Exception\InvalidConfigException;
use PWF\Input\HttpInput;
use PWF\Kernel;
use PWF\Loader;

/**
 * Router aggregation, to use different Route class
 * depending of the script name
 *
 * @author vincent
 */
class RouterAggregate implements Router{
    /**
     * @var Loader
     */
    private $loader;
    
    private $config;
    
    public function __construct(Loader $loader, Kernel $kernel) {
        $this->loader = $loader;
        $this->config = $kernel->conf('router', [
            'default_router' => HttpPathRouter::class
        ]);
    }

    public function getRoute(HttpInput $input) {
        $router = isset($this->config[$input->scriptName()], $this->config[$input->scriptName()]['router']) ? 
                $this->config[$input->scriptName()]['router'] : $this->config['default_router'];
        
        $router = $this->loader->getInstance($router);
        
        if(Constants::DEBUG && !($router instanceof Router))
            throw new InvalidConfigException('router.' . $input->scriptName() . '.router is not a valid Router class name');
        
        return $router->getRoute($input);
    }
}
