<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace PWF\Output;

/**
 * Output class for CLI apps
 * @author vincent
 */
class ConsoleOutput implements Output{
    /**
     *
     * @var ErrorFormater\ErrorFormater
     */
    private $errorFormater;
    
    private $code = Output::CODE_OK;
    
    public function __construct(ErrorFormater\ErrorFormater $errorFormater) {
        $this->errorFormater = $errorFormater;
    }
    
    public function getMimeType() {}

    public function removeHeader($header) {}

    public function setBody($body) {
        echo $body;
    }

    public function setCode($code) {
        $this->code = $code;
    }

    public function setError(\Exception $error) {
        echo $this->errorFormater->formatError($this, $error);
    }

    public function setHeader($header, $value) {}

    public function setMimeType($mime) {}
    
    public function hasError() {
        $base = (int)($this->code / 100);
        return $base !== 2 && $base !== 3;
    }
}

