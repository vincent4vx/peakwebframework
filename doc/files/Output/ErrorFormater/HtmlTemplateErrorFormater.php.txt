<?php

/*
 * The MIT License
 *
 * Copyright 2015 Vincent Quatrevieux <quatrevieux.vincent@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace PWF\Output\ErrorFormater;

/**
 * ErrorFormater using Template system
 * Don't forget to set this class into error_format config and create templates files
 *
 * @author Vincent Quatrevieux <quatrevieux.vincent@gmail.com>
 */
class HtmlTemplateErrorFormater implements ErrorFormater{
    /**
     *
     * @var \PWF\Output\Template\Template
     */
    private $template;
    
    private $config;
    
    public function __construct(\PWF\Output\Template\Template $template, \PWF\Kernel $kernel) {
        $this->template = $template;
        $this->config = $kernel->conf('template.errors', [
            'default' => 'error/default.html.php',
            'http' => [
                'default' => 'error/http.html.php',
            ],
            'custom' => []
        ]);
    }

    
    public function formatError(\PWF\Output\Output $output, \Exception $error) {
        $className = get_class($error);
        
        if(isset($this->config['custom'][$className])){
            $tpl = $this->config['custom'][$className];
        }elseif($error instanceof \PWF\Output\HttpException){
            $this->template->setTitle('Erreur ' . $error->getCode());
            if(isset($this->config['http'][$error->getCode()]))
                $tpl = $this->config['http'][$error->getCode()];
            else
                $tpl = $this->config['http']['default'];
        }else{
            $tpl = $this->config['default'];
        }
        
        echo $this->template->render($tpl, ['error' => $error]);
    }
}

