<?php

/*
 * The MIT License
 *
 * Copyright 2016 vincent.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace PWF\Manager;

use PWF\Constants;
use PWF\Kernel;
use PWF\Loader;
use PWF\MyBBKernel;
use PWF\WebKernel;
use PWF\CLIKernel;

/**
 * Create and save instance of Kernels
 *
 * @author vincent
 */
class KernelManager {
    static private $instances = [];
    
    /**
     * Retrieve or create the instance of the kernel
     * @param string $kernelClass The Kernel class name
     * @param string $name The instance name
     * @param array $config The configuration
     * @return Kernel The Kernel instance
     * @throws \InvalidArgumentException When $kernelClass is not a subclass of Kernel
     */
    static public function getOrCreateKernel($kernelClass, $name, array $config){
        if(isset(self::$instances[$name]))
            return self::$instances[$name];
        
        if(Constants::DEBUG){
            if(!is_subclass_of($kernelClass, Kernel::class, true)){
                throw new \InvalidArgumentException($kernelClass . ' is not a Kernel class');
            }    
        }
        
        $loader = new Loader();
        $kernel = new $kernelClass($loader, $config);
        
        self::$instances[$name] = $kernel;
        return $kernel;
    }
    
    /**
     * Load or retrieve an instance of MyBBKernel
     * @param string $name The instance name
     * @param array $config The configuration
     * @return MyBBKernel
     */
    static public function getOrCeateMyBBKernel($name, array $config){
        return self::getOrCreateKernel(MyBBKernel::class, $name, $config);
    }
    
    /**
     * Load or retrieve an instance of WebKernel
     * @param string $name The instance name
     * @param array $config The configuration
     * @return WebKernel
     */
    static public function getOrCeateWebKernel($name, array $config){
        return self::getOrCreateKernel(WebKernel::class, $name, $config);
    }
    
    /**
     * Load or retrieve an instance of CLIKernel
     * @param string $name The instance name
     * @param array $config The configuration
     * @return CLIKernel
     */
    static public function getOrCeateCLIKernel($name, array $config){
        return self::getOrCreateKernel(CLIKernel::class, $name, $config);
    }
}

