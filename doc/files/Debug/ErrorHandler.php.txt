<?php

/*
 * The MIT License
 *
 * Copyright 2016 vincent.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace PWF\Debug;

use PWF\Constants;
use PWF\Helper\Errors;
use PWF\Output\HttpException;
use PWF\Output\Output;
use PWF\Output\OutputStack;

/**
 * Description of ErrorHandler
 *
 * @author vincent
 */
class ErrorHandler {
    /**
     * @var OutputStack
     */
    private $output;
    
    /**
     * @var Errors
     */
    private $helper;
    
    private $showErrors;
    
    public function __construct(OutputStack $output, Errors $helper) {
        $this->output = $output;
        $this->helper = $helper;
        $this->showErrors = Constants::DEBUG;
        
        register_shutdown_function(function(){
            $error = error_get_last();
            
            if(!$error || !$this->isFatal($error)){
                return;
            }
            
            $this->showError($error);
        });
        
        set_error_handler([$this, 'handleError']);
        set_exception_handler([$this, 'showException']);
    }
    
    public function getShowErrors() {
        return $this->showErrors;
    }

    public function setShowErrors($showErrors) {
        $this->showErrors = $showErrors;
    }
    
    public function showException(\Exception $e){
        if($this->showErrors)
            $this->output->setError($e);
        else
            $this->output->setError(new HttpException('Internal server error', Output::CODE_INTERNAL_ERROR));
    }
    
    public function getTrace(){
        $trace = debug_backtrace();
        
        while(!empty($trace) && isset($trace[0]['class'])
                && $trace[0]['class'] === self::class)
            array_shift($trace);
        
        return $trace;
    }
    
    public function showError(array $error){
        if(!$this->isFatal($error) && (error_reporting() & $error['type']) != $error['type'])
            return;
        
        $trace = $this->getTrace();
        
        if($this->showErrors && (error_reporting() & $error['type']) == $error['type'])
            $this->output->setError($this->helper->createFromError($error, $this->getTrace()));
        else
            $this->output->setError(new HttpException('Internal server error', Output::CODE_INTERNAL_ERROR));
        
        if($this->isFatal($error))
            exit;
    }
    
    public function handleError($errno, $errstr, $errfile, $errline, $errcontext){
        $this->showError([
            'type' => $errno,
            'message' => $errstr,
            'file' => $errfile,
            'line' => $errline,
            'context' => $errcontext
        ]);
    }
    
    private function isFatal(array $error){
        return in_array($error['type'], [E_ERROR, E_COMPILE_ERROR, E_CORE_ERROR, E_USER_ERROR, E_PARSE]);
    }
}
