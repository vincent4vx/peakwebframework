<?php

/*
 * The MIT License
 *
 * Copyright 2016 vincent.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace PWF\Helper;

/**
 * 
 * @author vincent
 */
class Debug {
    public function showErrorFile($errorFile, $errorLine){
        if(strpos($errorFile, "eval()'d code") !== false){
            return $this->showEvalCode($errorFile, $errorLine);
        }
        
        $code = '';
        $file = file($errorFile);
        $lineStart = $errorLine - 3;
        
        if($lineStart < 0)
            $lineStart = 0;
        
        for($line = $lineStart; $line < $errorLine + 2 && $line < count($file); ++$line){
            $code .= ($line + 1) . '. ' . $file[$line];
        }
        
        $this->showCode($code, $errorLine);
    }
    
    public function showEvalCode($errorFile, $errorLine){
        $matches = [];
        
        if(!preg_match("#(.*)\((\d+)\) : eval\(\)'d#iU", $errorFile, $matches)) //not match ? 
            return;
        
        $realFile = $matches[1];
        $realLine = $matches[2];
        
        $this->showErrorFile($realFile, $realLine);
    }
    
    public function showCode($code, $errorLine){
        $openTag = strpos($code, '<?');
        $closeTag = false;
        $openTagAdded = false;
        
        if($openTag !== false){
            $closeTag = strpos($code, '?>');
        }
        
        //There is not open tag or there is a close tag before the open (i.e. The open tag is missing)
        if($openTag === false
            || ($closeTag !== false && $closeTag < $openTag)){
            $code = '<?php' . PHP_EOL . $code; //Add the open tag
            $openTagAdded = true;
        }
        
        $str = highlight_string($code, true);
        
        $str = preg_replace('#' . $errorLine . '\..*<br\s*/?>#iU', '<strong style="background: rgba(0,0,0,.1);">$0</strong>', $str);
        
        if($openTagAdded)
            $str = str_replace('&lt;?php<br />', '', $str);
        
        echo $str;
    }
    
    public function showTrace(array $trace){
        echo '<table class="debug trace">';
        
        echo '<tr><th>#</th><th>File</th><th>Function</th></tr>';
        
        foreach($trace as $i => $line){
            echo '<tr>';
            
            echo "<td>#{$i}</td>";
            
            if(isset($line['file'], $line['line'])){
                echo '<td class="debug file">', htmlentities($line['file']), ':', $line['line'];

                echo $this->showErrorFile($line['file'], $line['line']);

                echo '</td>';
            }else{
                echo '<td>[PHP]</td>';
            }
            
            echo '<td class="debug function">';
            
            if(isset($line['class'], $line['type']))
                echo htmlentities($line['class']), $line['type'];
            
            echo $line['function'], '()</td>';
            
            echo '</tr>';
        }
        
        echo '</table>';
    }
}

