<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace PWF\Exception;

/**
 * Exception throws when an object is in invalid state
 * (i.e. some attributes are not initialized)
 * @author vincent
 */
class InvalidStateException extends \RuntimeException{
    public function __construct($message, $code = 0, $previous = null) {
        parent::__construct($message, $code, $previous);
    }
}

