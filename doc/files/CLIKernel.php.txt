<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace PWF;

/**
 * Kernel for CLI php apps
 *
 * @author vincent
 * 
 */
class CLIKernel extends AbstractKernel{
    public function __construct(Loader $loader, array $config) {
        parent::__construct($loader, $config);
        
        $this->router = $this->loader->getInstance(Router\Router::class);
        $this->input = $this->loader->getInstance(Input\HttpInput::class);
    }
    
    protected function getDefaultModules() {
        return array_merge(parent::getDefaultModules(), [
            Output\ErrorFormater\ErrorFormater::class => Output\ErrorFormater\PlainTextErrorFormater::class,
            Input\HttpInput::class => Input\CLIInput::class,
            Router\Router::class => Router\HttpPathRouter::class,
            Output\Output::class => Output\ConsoleOutput::class
        ]);
    }
}

